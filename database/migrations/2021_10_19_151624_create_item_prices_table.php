<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_prices', function (Blueprint $table) {
            $table->id();
            $table->integer('item_id');
            $table->integer('branch_id')->nullable();
            $table->float('current_price')->nullable();
            $table->float('discount')->nullable();
            $table->float('discount_percent')->nullable();
            $table->enum('discount_unit', ['1', '0']);
            $table->float('discount_min')->nullable();
            $table->date('date_discount')->nullable();
            $table->date('date_end_discount')->nullable();
            $table->enum('status', ['1', '0']);
            $table->char('created_by')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_prices');
    }
}
