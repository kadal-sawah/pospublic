<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_profiles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('bussiness_type');
            $table->char('npwp_number');
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->char('telepon_1');
            $table->char('telepon_2')->nullable();
            $table->char('fax')->nullable();
            $table->char('bank_name');
            $table->char('bank_rekening_number');
            $table->string('bank_owner');
            $table->integer('country_id');
            $table->integer('province_id');
            $table->integer('city_id');
            $table->integer('district_id');
            $table->integer('village_id');
            $table->string('website');
            $table->string('email');
            $table->enum('status', ['1', '0']);
            $table->timestamp('deleted_at')->nullable();
            $table->char('created_by')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_profiles');
    }
}
