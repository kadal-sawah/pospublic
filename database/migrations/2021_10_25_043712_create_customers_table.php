<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('type_id');
            $table->string('address');
            $table->string('address_billing');
            $table->string('address_delivery');
            $table->string('contact_name_1');
            $table->string('contact_name_2');
            $table->string('contact_name_3');
            $table->string('contact_name_4');
            $table->char('postal_code');
            $table->integer('province_id');
            $table->integer('city_id');
            $table->integer('district_id');
            $table->integer('village_id');
            $table->char('telepon_1');
            $table->char('telepon_2');
            $table->char('facsimile');
            $table->string('email');
            $table->char('bank_name');
            $table->char('bank_rekening_number');
            $table->string('bank_owner');
            $table->enum('status', ['1', '0']);
            $table->string('photo');
            $table->char('created_by')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
