@extends('layouts.app')

@once
@push('page_css')
    <link rel="stylesheet" href="{{ asset('css/dataTables/dataTables.bootstrap4.min.css') }}">
@endpush
@endonce

@section('content')

    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Item Master
        </h2>

        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <button style="border: none" name="total" class="info-box-icon bg-primary elevation-1"><i class="fas fa-user-cog"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Total</span>
                        <span class="info-box-number" name="all">0
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="active" class="info-box-icon bg-success elevation-1"><i class="fas fa-thumbs-up"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Aktif</span>
                        <span class="info-box-number" name="active">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="inactive" class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-down"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Tidak Aktif</span>
                        <span class="info-box-number" name="inactive">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="trashed" class="info-box-icon bg-info elevation-1"><i class="fas fa-recycle"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Recycle</span>
                        <span class="info-box-number" name="trashed">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Item
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-item"><i class="fas fa-plus"></i> Add</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="delete"><i
                                        class="fas fa-trash"></i>
                                    Delete</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-2">
                        <table class="table table-head-fixed text-nowrap" id="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="customCheckbox"
                                                onchange="checkbox_all(this)">
                                            <label for="customCheckbox" class="custom-control-label"></label>
                                        </div>
                                    </th>
                                    <th>Name</th>
                                    <th>Code</th>
                                    <th>Photo</th>
                                    <th>Brand</th>
                                    <th>Consignment</th>
                                    <th>Created by</th>
                                    <th>Status</th>
                                    <th>Created at</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <!-- insert data -->
    <div class="modal fade" id="add-item" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="insert-item" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name <i class="text-danger">*</i></label>
                        <input required type="text" class="form-control" name="item_name" placeholder="Enter item name">
                    </div>
                    <div class="form-group">
                        <label for="item_code">Code <i class="text-danger">*</i></label>
                        <input required type="text" class="form-control" name="item_code" placeholder="item code">
                    </div>
                    <!-- <div class="form-group">
                        <label for="group_id">Group</label>
                        <select class="form-control" name="group_id">
					        <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div> -->
                    <div class="form-group">
                        <label for="item_group_id">Item Group <i class="text-danger">*</i></label>
                        <a href="{{ route('item-group') }}" class=" float-right btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add New Group</a>
                        <select required class="form-control" name="item_group_id" id="select_group">
					            <option value="">== Pilih Kelompok ==</option>
                            @foreach ($item_group as $group)
                                <option value="{{$group->id}}">{{$group->group_name}}</option>
                            @endforeach
                        </select>
                    </div>
                        <label for="photo">Photo</label>
                    <div class="custom-file mb-3">
                        <input type="file" class="custom-file-input undefined" name="photo" value="{{ old('photo') }}">
                        <label class="custom-file-label">{{ old('photo') }}</label>
                    </div>
                    <div class="form-group">
                        <label for="description">Description <i class="text-danger">*</i></label>
                        <textarea required class="form-control" name="description" placeholder="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="brand_id">Brand <i class="text-danger">*</i></label>
                        <a href="#" class=" float-right btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add New Brand</a>
                        <select required class="form-control" name="brand_id">
					            <option value="">== Pilih Merek ==</option>
                            @foreach ($merk as $m)
                                <option value="{{$m->id}}">{{$m->brand_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stock_min">Stock Min <i class="text-danger">*</i></label>
                        <input required type="number" class="form-control" name="stock_min" placeholder="stock min">
                    </div>
                    <div class="form-group">
                        <label for="stock_max">Stock Max <i class="text-danger">*</i></label>
                        <input required type="number" class="form-control" name="stock_max" placeholder="Stock Max">
                    </div>
                    <div class="form-group">
                        <label for="consignment">Consignment <i class="text-danger">*</i></label>
                        <input required type="text" class="form-control" name="consignment" placeholder="Consignment">
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="load">Save</button>
                    </div>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- update data -->
    <div class="modal fade" id="edit-item" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="update-item" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Item</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Name <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="item_name" placeholder="Enter item name">
                        </div>
                        <div class="form-group">
                            <label for="code_group">Code <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="item_code" placeholder="Enter code item">
                            <input type="hidden" class="form-control" name="id" placeholder="ID Item">
                        </div>
                        <div class="form-group">
                            <label for="item_group_id">Item Group <i class="text-danger">*</i></label>
                            <select required class="form-control" name="item_group_id">
                                @foreach ($item_group as $group)
                                    <option 
                                        value="{{$group->id}}"
                                    >
                                    {{$group->group_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="photo">Photo</label>
                            <table align="center" class="mb-2">
                                <tr>
                                    <td><img src="" id="pict" style="max-width: 350px; max-height: 250px;" class="img-holder align-items-center img-fluid rounded-start" alt="..." /></td>
                                </tr>
                            </table>
                                <p><input type="file" class="form-control" name="ganti_photo"></p>
                        </div>
                        <!-- <div class="form-group">
                            <label for="">Product image</label>
                            <input type="file" name="product_image" class="form-control">
                            <span class="text-danger error-text product_image_error"></span>
                        </div>
                        <div class="img-holder"></div> -->
                        <div class="form-group">
                            <label for="description">Description <i class="text-danger">*</i></label>
                            <textarea required class="form-control" name="description" placeholder="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="brand_id">Brand <i class="text-danger">*</i></label>
                            <select required class="form-control" name="brand_id">
                                @foreach ($merk as $m)
                                    <option 
                                        value="{{$m->id}}"
                                    >
                                    {{$m->brand_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="stock_min">Stock Min <i class="text-danger">*</i></label>
                            <input required type="number" class="form-control" name="stock_min" placeholder="stock min" >
                        </div>
                        <div class="form-group">
                            <label for="stock_max">Stock Max <i class="text-danger">*</i></label>
                            <input required type="number" class="form-control" name="stock_max" placeholder="Stock Max" >
                        </div>
                        <div class="form-group">
                            <label for="consignment">Consignment <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="consignment" placeholder="Consignment" >
                        </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="updating">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@once

    @push('page_scripts')
        <script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/dataTables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="https://adminlte.io/themes/v3/dist/js/adminlte.min.js"></script>
        <script src="https://adminlte.io/themes/v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        <script src="{{ asset('js/core.js') }}" defer></script>
        <script src="{{ asset('js/item.js') }}" defer></script>
    @endpush

@endonce
