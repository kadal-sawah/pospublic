            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" value="{{ $data->id }}" name="id" id="id_data" />
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Enter item name" value="{{ $data->name }}">
                    </div>
                    <div>
                        <label for="status">Status</label>
                        <input type="text" class="form-control" name="status" placeholder="Status" value="{{ $data->status }}">
                    </div>
                </div>
            </div>