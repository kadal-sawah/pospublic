@extends('layouts.app')

@once
@push('page_css')
<link rel="stylesheet" href="{{ asset('css/dataTables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
@endpush
@endonce

@section('content')

    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Customer
        </h2>

        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <button style="border: none" name="total" class="info-box-icon bg-primary elevation-1"><i class="fas fa-user-cog"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Total</span>
                        <span class="info-box-number" name="all">0
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="active" class="info-box-icon bg-success elevation-1"><i class="fas fa-thumbs-up"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Aktif</span>
                        <span class="info-box-number" name="active">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="inactive" class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-down"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Tidak Aktif</span>
                        <span class="info-box-number" name="inactive">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="trashed" class="info-box-icon bg-info elevation-1"><i class="fas fa-recycle"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Recycle</span>
                        <span class="info-box-number" name="trashed">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Customer
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button href="javascript:void(0)" class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-suppliertype" id="createNewCustomer"><i class="fas fa-plus"></i> Add</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="delete"><i
                                        class="fas fa-trash"></i>
                                    Delete</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive">
                        <table class="table table-bordered table-hovered table-sm" id="table">
                            <thead class="thead-light text-center">
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="customCheckbox"
                                                onchange="checkbox_all(this)">
                                            <label for="customCheckbox" class="custom-control-label"></label>
                                        </div>
                                    </th>
                                    <th>Title</th>
                                    <th>Tipe</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th>Update At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="ajaxModel" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="CustomerForm" name="CustomerForm">
                    <input type="hidden" name="Customer_id" id="Customer_id">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modelHeading">Tambah Customer</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Masukan title">
                        </div>
                        <div class="form-group">
                            <label for="first_name">Nama Depan</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Masukan nama depan">
                        </div>
                        <div class="form-group">
                            <label for="last_name">Nama Belakang</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Masukan nama belakang">
                        </div>
                        <div class="form-group">
                            <label for="tgl_lahir">Tanggal Lahir</label>
                            <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect2">Tipe Customer</label>
                            <br>
                            <select required class="form-control selectpicker" data-live-search="true" id="type_id" name="type_id" id="exampleFormControlSelect2">
                              <option value="">-- Pilih Tipe Customer --</option>
                            @foreach($customertype as $customertype)
                              <option value="{{ $customertype->id }}">{{ $customertype->type_name }}</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat</label>
                            <textarea class="form-control" rows="3" id="address" name="address" placeholder="Masukan alamat"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="address_billing">Alamat Penagihan</label>
                            <textarea class="form-control" rows="3" id="address_billing" name="address_billing" placeholder="Masukan alamat penagihan"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="address_delivery">Alamat Pengiriman</label>
                            <textarea class="form-control" rows="3" id="address_delivery" name="address_delivery" placeholder="masukan alamat pengiriman"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="contact_name_1">Nama Kontak 1</label>
                            <input type="text" class="form-control" id="contact_name_1" name="contact_name_1" placeholder="Masukan nama kontak 1">
                        </div>
                        <div class="form-group">
                            <label for="contact_name_2">Nama Kontak 2</label>
                            <input type="text" class="form-control" id="contact_name_2" name="contact_name_2" placeholder="Masukan nama kontak 2">
                        </div>
                        <div class="form-group">
                            <label for="contact_name_3">Nama Kontak 3</label>
                            <input type="text" class="form-control" id="contact_name_3" name="contact_name_3" placeholder="Masukan nama kontak 3">
                        </div>
                        <div class="form-group">
                            <label for="contact_name_4">Nama Kontak 4</label>
                            <input type="text" class="form-control" id="contact_name_4" name="contact_name_4" placeholder="Masukan nama kontak 4">
                        </div>
                        <div class="form-group">
                            <label for="postal_code">Kode Pos</label>
                            <input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Masukan kode pos">
                        </div>
                        <div class="form-group">
                            <label for="province_id">Provinsi</label>
                            <br>
                                @php
                                    $provinces = new App\Http\Controllers\CustomerController;
                                    $provinces= $provinces->provinces();
                                @endphp
                                <select class="form-control" name="province_id" id="province_id" required>
                                    <option>==Pilih Salah Satu==</option>
                                    @foreach ($provinces as $item)
                                        <option value="{{ $item->id ?? '' }}">{{ $item->name ?? '' }}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="form-group">
                            <label for="city_id">Kabupaten / Kota</label>
                            <br>
                                @php
                                    $provinces = new App\Http\Controllers\CustomerController;
                                    $provinces= $provinces->provinces();
                                @endphp
                                <select class="form-control" name="city_id" id="city_id" required>
                                    <option>==Pilih Salah Satu==</option>
                                </select>
                        </div>
                        <div class="form-group">
                            <label for="district_id">Kecamatan</label>
                            <br>
                                <select class="form-control" name="district_id" id="district_id" required>
                                    <option>==Pilih Salah Satu==</option>
                                </select>
                        </div>
                        <div class="form-group">
                            <label for="village_id">Desa</label>
                                <select class="form-control" name="village_id" id="village_id" required>
                                    <option>==Pilih Salah Satu==</option>
                                </select>
                        </div>
                        <div class="form-group">
                            <label for="telepon_1">Telepon 1</label>
                            <input type="text" class="form-control" id="telepon_1" name="telepon_1" placeholder="Masukan telepon 1">
                        </div>
                        <div class="form-group">
                            <label for="telepon_2">Telepon 2</label>
                            <input type="text" class="form-control" id="telepon_2" name="telepon_2" placeholder="Masukan telepon 2">
                        </div>
                        <div class="form-group">
                            <label for="facsimile">Faksimil</label>
                            <input type="text" class="form-control" id="facsimile" name="facsimile" placeholder="Masukan faksimil">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Masukan email">
                        </div>
                        <div class="form-group">
                            <label for="bank_name">Nama Bank</label>
                            <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Masukan nama bank">
                        </div>
                        <div class="form-group">
                            <label for="bank_rekening_number">Nomor Rekening</label>
                            <input type="text" class="form-control" id="bank_rekening_number" name="bank_rekening_number" placeholder="Masukan nomor rekening">
                        </div>
                        <div class="form-group">
                            <label for="bank_owner">Nama Pemilik Rekening</label>
                            <input type="text" class="form-control" id="bank_owner" name="bank_owner" placeholder="Masukan nama pemilik rekening">
                        </div>
                        <div class="form-group">
                        <label for="type_id">Foto</label>
                        <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="photo">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="photo" name="photo" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01">Pilih file</label>
                        </div>
                        </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="saveBtn">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@once

    @push('page_scripts')

         <script>
        function onChangeSelect(url, id, name) {
            // send ajax request to get the cities of the selected province and append to the select tag
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    id: id
                },
                success: function (data) {
                    $('#' + name).empty();
                    $('#' + name).append('<option>==Pilih Salah Satu==</option>');

                    $.each(data, function (key, value) {
                        $('#' + name).append('<option value="' + key + '">' + value + '</option>');
                    });
                }
            });
        }
        $(function () {
            $('#province_id').on('change', function () {
                onChangeSelect('{{ route("cities") }}', $(this).val(), 'city_id');
            });
            $('#city_id').on('change', function () {
                onChangeSelect('{{ route("districts") }}', $(this).val(), 'district_id');
            })
            $('#district_id').on('change', function () {
                onChangeSelect('{{ route("villages") }}', $(this).val(), 'village_id');
            })
        });
    </script>


        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/dataTables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="https://adminlte.io/themes/v3/dist/js/adminlte.min.js"></script>
        <script src="https://adminlte.io/themes/v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="{{ asset('js/core.js') }}" defer></script>
        <script src="{{ asset('js/page/customer/customer.js') }}" defer></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js') }}"></script>
    @endpush

@endonce
