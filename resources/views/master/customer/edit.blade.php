<div class="form-group">
    <label for="title">Title</label>
    <input type="hidden" name="id" value="{{ $customer->id }}" id="id_data">
    <input type="text" class="form-control" name="title" value="{{ $customer->title }}" placeholder="Masukan title">
</div>
<div class="form-group">
    <label for="first_name">Nama Depan</label>
    <input type="text" class="form-control" name="first_name" placeholder="Masukan nama depan" value="{{ $customer->first_name }}">
</div>
<div class="form-group">
    <label for="last_name">Nama Belakang</label>
    <input type="text" class="form-control" name="last_name" placeholder="Masukan nama belakang" value="{{ $customer->last_name }}">
</div>
<div class="form-group">
    <label for="tgl_lahir">Tanggal Lahir</label>
    <input type="date" class="form-control" name="tgl_lahir">
</div>
<div class="form-group">
    <label for="name">Tipe Customer</label>
    <select class="form-control" name="type_id">
    @foreach($customertype as $customertype)
        <option value="{{ $customertype->id }}" <?php if($customer->type_id == $customertype->id){ echo 'selected';} ?>>{{ $customertype->type_name }}</option>
    @endforeach
    </select>
</div>
<div class="form-group">
    <label for="address">Alamat</label>
    <textarea class="form-control" rows="3" name="address" placeholder="Masukan alamat">{{ $customer->address }}</textarea>
</div>
<div class="form-group">
    <label for="address_billing">Alamat Penagihan</label>
    <textarea class="form-control" rows="3" name="address_billing" placeholder="Masukan alamat penagihan">{{ $customer->address_billing }}</textarea>
</div>
<div class="form-group">
    <label for="address_delivery">Alamat Pengiriman</label>
    <textarea class="form-control" rows="3" name="address_delivery" placeholder="masukan alamat pengiriman">{{ $customer->address_delivery }}</textarea>
</div>
<div class="form-group">
    <label for="contact_name_1">Nama Kontak 1</label>
    <input type="text" class="form-control" name="contact_name_1" placeholder="Masukan nama kontak 1" value="{{  $customer->contact_name_1 }}">
</div>
<div class="form-group">
    <label for="contact_name_2">Nama Kontak 2</label>
    <input type="text" class="form-control" name="contact_name_2" placeholder="Masukan nama kontak 2" value="{{ $customer->contact_name_2 }}">
</div>
<div class="form-group">
    <label for="contact_name_3">Nama Kontak 3</label>
    <input type="text" class="form-control" name="contact_name_3" placeholder="Masukan nama kontak 3" value="{{ $customer->contact_name_3 }}">
</div>
<div class="form-group">
    <label for="contact_name_4">Nama Kontak 4</label>
    <input type="text" class="form-control" name="contact_name_4" placeholder="Masukan nama kontak 4" value="{{ $customer->contact_name_4 }}">
</div>
<div class="form-group">
    <label for="postal_code">Kode Pos</label>
    <input type="text" class="form-control" name="postal_code" placeholder="Masukan kode pos" value="{{ $customer->postal_code }}">
</div>
<div class="form-group">
    <label for="provinsi">Provinsi</label>
        <br>
        @php
            $provinces = new App\Http\Controllers\CustomerController;
            $provinces= $provinces->provinces();
        @endphp
    <select class="form-control" name="province_id" id="provinsi" required>
        <option>==Pilih Salah Satu==</option>
            @foreach ($provinces as $item)
                <option value="{{ $item->id }}" <?php if($customer->province_id == $item->id){ echo 'selected';} ?>>{{ $item->name }}</option>
            @endforeach
    </select>
</div>
<div class="form-group">
    <label for="kota">Kabupaten / Kota</label>
        <br>
    <select class="form-control" name="city_id" id="kota" required>
        <option>==Pilih Salah Satu==</option>
    </select>
</div>
<div class="form-group">
    <label for="kecamatan">Kecamatan</label>
        <br>
    <select class="form-control" name="district_id" id="kecamatan" required>
        <option>==Pilih Salah Satu==</option>
    </select>
</div>
<div class="form-group">
    <label for="desa">Desa</label>
        <select class="form-control" name="village_id" id="desa" required>
            <option>==Pilih Salah Satu==</option>
        </select>
</div>
<div class="form-group">
    <label for="telepon_1">Telepon 1</label>
        <input type="text" class="form-control" name="telepon_1" placeholder="Masukan telepon 1">
</div>
<div class="form-group">
    <label for="telepon_2">Telepon 2</label>
        <input type="text" class="form-control" name="telepon_2" placeholder="Masukan telepon 2">
</div>
<div class="form-group">
    <label for="facsimile">Faksimil</label>
        <input type="text" class="form-control" name="facsimile" placeholder="Masukan faksimil">
</div>
<div class="form-group">
    <label for="email">Email</label>
        <input type="text" class="form-control" name="email" placeholder="Masukan email">
</div>
<div class="form-group">
    <label for="bank_name">Nama Bank</label>
        <input type="text" class="form-control" name="bank_name" placeholder="Masukan nama bank">
</div>
<div class="form-group">
    <label for="bank_rekening_number">Nomor Rekening</label>
        <input type="text" class="form-control" name="bank_rekening_number" placeholder="Masukan nomor rekening">
</div>
<div class="form-group">
    <label for="bank_owner">Nama Pemilik Rekening</label>
        <input type="text" class="form-control" name="bank_owner" placeholder="Masukan nama pemilik rekening">
</div>
<div class="form-group">
    <label for="type_id">Foto</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
            </div>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="inputGroupFile01" name="photo" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="inputGroupFile01">Pilih file</label>
            </div>
        </div>
</div>
