@extends('layouts.app')

@once
@push('page_css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
@endonce

@section('third_party_scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
@endsection

@section('content')

<div class="container-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        {{ $data['menuTitle'] ?? '' }}
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                        {!! $data['breadcrumbs'] ?? '' !!}
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="row">
        <div class="col-12">

            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">{{ $data['collapseTitle'] }}</h3>
                    <div class=" card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Messages</span>
                                    <span class="info-box-number">1,410</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Messages</span>
                                    <span class="info-box-number">1,410</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Messages</span>
                                    <span class="info-box-number">1,410</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Messages</span>
                                    <span class="info-box-number">1,410</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">{{ $data['tableTitle'] ?? '' }}</h3>
                    <div class="card-tools">
                        <!-- Expand Button -->
                        <!-- <button type="button" class="btn btn-tool" data-card-widget="maximize">
                            <i class="fas fa-expand"></i>
                        </button> -->
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="container-fluid">
                    <a class="btn btn-app bg-warning disabled" href="javascript: void(0)" id="buttonSampah">
                        <span class="badge bg-info">0</span>
                        <i class="fas fa-trash-restore-alt"></i> Sampah (box)
                    </a>
                    <a class="btn btn-app bg-danger disabled"  href="javascript: void(0)" id="buttonSoftSampah">
                        <span class="badge bg-warning">0</span>
                        <i class="fas fa-trash"></i> Hapus (box)
                    </a>
                    <a class="btn btn-app bg-outline-warnning">
                        <span class="badge bg-danger">0</span>
                        <i class="fas fa-trash-restore-alt"></i> Sampah
                    </a>
                    <a class="btn btn-app bg-secondary">
                        <span class="badge bg-success">0</span>
                        <i class="fas fa-barcode"></i> Merk
                    </a>
                    </div>
                    <div class="container-fluid border border-dark rounded p-4 table-responsive">
                        <table class="table table-sm table-head-fixed text-nowrap table-hover table-striped" id="merk-table">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="chkCheckAll"></th>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Kode</th>
                                    <th>Dibuat oleh</th>
                                    <th>Dibuat</th>
                                    <th>Terakhir Pembaruan</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Form Merk</h3>
                    <div class="card-tools">
                        <!-- Collapse Button -->
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                
                <div class="card-body">
                    <div class="container-fluid">
                        <form id="formMerk" action="{{ route('storeMerk') }}" name="formMerk">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-sm-12 col-md-8 col-lg-6">
                                    <label for="namaMerek">Nama Merek<sup class="text-danger ml-2">*</sup></label>
                                    <input type="text" class="form-control" id="namaMerek" placeholder="masukan nama merek">
                                </div>
                                <div class="form-group col-sm-12 col-md-8 col-lg-6">
                                    <label for="codeMerek">Code Merek<sup class="text-danger ml-2">*</sup></label>
                                    <input type="text" class="form-control" id="codeMerek" placeholder="masukan kode merek">
                                </div>
                                <div class="form-group col-6">
                                    <label for="dibuatOleh">Dibuat oleh</label>
                                    <input type="email" class="form-control" id="dibuatOleh" value="{{ Auth::user()->email }}" readonly>
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="statusMerek" checked>
                                    <label class="form-check-label" for="statusMerek">Aktif</label>
                                </div>
                                <div class="form-group col-12">
                                    <button type="submit" class="btn btn-primary btn-flat" id="simpanMerk">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <p class="text-muted"><sup class="text-danger">*</sup> : Harus Di isi</p>
                </div>
            </div>

        </div>
    </div>

</div>

@endsection

@once

@push('page_scripts')
<script src="{{ asset('js/page/merk/merk.js') }}"></script>
@endpush

@endonce