            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" value="{{ $data->id }}" name="id" id="id_data" />
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="item_name" placeholder="Enter item name" value="{{ $data->item_name }}">
                    </div>
                    <div class="form-group">
                        <label for="item_code">Code</label>
                        <input type="text" class="form-control" name="item_code" placeholder="item code" value="{{ $data->item_code }}">
                    </div>
                    <div class="form-group">
                        <label for="group_id">Group</label>
                        <select class="form-control" name="group_id">
					        <option value="{{ $data->group_id }}">{{ $data->group_id }}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="item_group_id">Item Group</label>
                        <select class="form-control" name="item_group_id">
					        @foreach ($item_group as $group)
                                <option 
                                    value="{{$group->id}}"
	                                @if ($group->id === $data->item_group_id)
	                                selected
	                            @endif
                                >
                                {{$group->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="photo">Photo</label>
                        <div class="row col-sm-12">
                            <img src="{{asset('storage/'.$data->photo)}}"  class="img-fluid rounded-start col-sm-6" alt="...">
                            <input type="file" class="form-control col-sm-6" name="photo" placeholder="photo" value="{{ old('photo') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" placeholder="description"><?php echo $data->description ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="brand_id">Brand</label>
                        <select class="form-control" name="brand_id">
					        @foreach ($merek as $merk)
                                <option 
                                    value="{{$merk->id}}"
	                                @if ($merk->id === $data->brand_id)
	                                selected
	                            @endif
                                >
                                {{$merk->brand_name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stock_min">Stock Min</label>
                        <input type="text" class="form-control" name="stock_min" placeholder="stock min" value="{{ $data->stock_min }}">
                    </div>
                    <div class="form-group">
                        <label for="stock_max">Stock Max</label>
                        <input type="text" class="form-control" name="stock_max" placeholder="Stock Max" value="{{ $data->stock_max }}">
                    </div>
                    <div class="form-group">
                        <label for="consignment">Consignment</label>
                        <input type="text" class="form-control" name="consignment" placeholder="Consignment" value="{{ $data->consignment }}">
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <input type="text" class="form-control" name="status" placeholder="Status" value="{{ $data->status }}">
                    </div>
                </div>
            </div>