@extends('layouts.app')

@once
@push('page_css')
<link rel="stylesheet" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@endonce

@section('third_party_scripts')
<script src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
@endsection

@section('content')
    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Master Barang
        </h2>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Barang
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-item"><i class="fas fa-plus"></i> Add</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="deleteAllSelectedRecords"><i class="fas fa-trash"></i>
                                    Delete Selected</button>
                                
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0 table-hover" >
                        <table class="table table-head-fixed text-nowrap" id="barang">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="chkCheckAll" /></th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Code</th>
                                    <th>Group</th>
                                    <th>Item Group</th>
                                    <th>Photo</th>
                                    <th>Brand</th>
                                    <th>Status</th>
                                    <th>Created by</th>
                                    <th>Created at</th>
                                    <th>Modify</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($item as $data)
                                    <tr id="sid{{ $data->id }}">
                                        <td><input type="checkbox" name="ids" class="checkBoxClass" value="{{ $data->id }}" /></td>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->item_name }}</td>
                                        <td>{{ $data->item_code }}</td>
                                        <td>{{ $data->group_id }}</td>
                                        <td>{{ $data->nama_item_group }}</td>
                                        <td><img src="{{asset('storage/'.$data->photo)}}" style="max-width: 50px; max-height: 37px;" class="img-fluid rounded-start" alt="..."></td>
                                        <td>{{ $data->nama_merek }}</td>
                                        <td>{{ $data->status }}</td>
                                        <td>{{ $data->nama_user }}</td>
                                        <td>{{ $data->created_at }}</td>
                                        <td><a href='#' data-id="{{ $data->id }}" class="btn btn-success btn-sm btn-edit"><i class="nav-icon fas fa-edit"></i></a>
                                            <a href="{{ route('item-delete',['id'=>$data->id]) }}" onclick="return confirm('Yakin Hapus data')" class="btn btn-danger btn-sm">
                                                    <i class="nav-icon fas fa-trash"></i>
                                                  </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <!-- Modal Add -->
    <div class="modal fade" id="add-item" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('add-item') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="item_name" placeholder="Enter item name">
                    </div>
                    <div class="form-group">
                        <label for="item_code">Code</label>
                        <input type="text" class="form-control" name="item_code" placeholder="item code">
                    </div>
                    <div class="form-group">
                        <label for="group_id">Group</label>
                        <select class="form-control" name="group_id">
					        <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="item_group_id">Item Group</label>
                        <select class="form-control" name="item_group_id">
					        <option value="item_group_id"> Pilih Kelompok </option>
                            @foreach ($item_group as $group)
                                <option value="{{$group->id}}">{{$group->name}}</option>
                            @endforeach
                        </select>
                    </div>
                        <label for="photo">Photo</label>
                    <div class="custom-file mb-3">
                        <input type="file" class="custom-file-input undefined" name="photo" value="{{ old('photo') }}">
                        <label class="custom-file-label">{{ old('photo') }}</label>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" placeholder="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="brand_id">Brand</label>
                        <select class="form-control" name="brand_id">
					        <option value="brand_id"> Pilih Merek </option>
                            @foreach ($merek as $merk)
                                <option value="{{$merk->id}}">{{$merk->brand_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stock_min">Stock Min</label>
                        <input type="text" class="form-control" name="stock_min" placeholder="stock min">
                    </div>
                    <div class="form-group">
                        <label for="stock_max">Stock Max</label>
                        <input type="text" class="form-control" name="stock_max" placeholder="Stock Max">
                    </div>
                    <div class="form-group">
                        <label for="consignment">Consignment</label>
                        <input type="text" class="form-control" name="consignment" placeholder="Consignment">
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <input type="text" class="form-control" name="status" placeholder="Status">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Modal Update -->
    <div class="modal fade" id="modal-edit" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('add-item') }}" method="POST" id="form-edit" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-update">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@once
@push('page_scripts')
<script type="text/javascript" src="{{ asset('js/barang/barang.js') }}">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script>
    @if (Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @endif
<script>

@endpush
@endonce