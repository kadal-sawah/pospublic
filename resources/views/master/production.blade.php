@extends('layouts.app')

@once
@push('page_css')
<link rel="stylesheet" href="{{ asset('css/dataTables/dataTables.bootstrap4.min.css') }}">
@endpush
@endonce

@section('content')

    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Production
        </h2>

        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <button style="border: none" name="total" class="info-box-icon bg-primary elevation-1"><i class="fas fa-user-cog"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Total</span>
                        <span class="info-box-number" name="all">0
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="active" class="info-box-icon bg-success elevation-1"><i class="fas fa-thumbs-up"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Aktif</span>
                        <span class="info-box-number" name="active">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="inactive" class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-down"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Tidak Aktif</span>
                        <span class="info-box-number" name="inactive">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="trashed" class="info-box-icon bg-info elevation-1"><i class="fas fa-recycle"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Recycle</span>
                        <span class="info-box-number" name="trashed">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Production
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-production" ><i class="fas fa-plus"></i> Add</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="delete"><i
                                        class="fas fa-trash"></i>
                                    Delete</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-2">
                        <table class="table table-head-fixed text-nowrap" id="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="customCheckbox"
                                                onchange="checkbox_all(this)">
                                            <label for="customCheckbox" class="custom-control-label"></label>
                                        </div>
                                    </th>
                                    <th>Invoice</th>
                                    <th>Branch</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Created By</th>
                                    <th>Updated By</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <!-- insert data -->
    <div class="modal fade" id="add-production" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="insert-production">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Production</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="col-sm-12" id="dynamicAddRemove">
                        <input type="hidden" name="moreFields[0][created_by]" value="{{$user_id}}">
                        <input type="hidden" name="moreFields[0][updated_by]" value="{{$user_id}}">
                        <div class="form-group">
                            <label for="invoice">Invoice <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="moreFields[0][invoice]" placeholder="Enter Invoice">
                        </div>
                        <div class="form-group">
                            <label for="branch_id">branch <i class="text-danger">*</i></label>
                            <select required class="form-control" name="moreFields[0][branch_id]">
                                    <option value="" >== Select Item ==</option>
                                @foreach ($branch as $b)
                                    <option value="{{$b->id}}">
                                    {{$b->id}} - {{$b->branch_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">Description <i class="text-danger">*</i></label>
                            <textarea required class="form-control" name="moreFields[0][description]" placeholder="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <input required type="number" class="form-control" name="moreFields[0][amount]" placeholder="Enter code group">
                        </div>
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input required type="date" class="form-control" name="moreFields[0][date]" >
                        </div>
                        <div class="justify-content-between">
                            <button type="button" name="add" id="add-btn" class="btn btn-success col-sm-12">Add More Fields</button>
                        </div>
                        </table>
                    </div><br/>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="load">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- update data -->
    <div class="modal fade" id="edit-production" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="update-production">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit production</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" name="id" placeholder="ID production">
                        <div class="form-group">
                            <label for="invoice">Invoice <i class="text-danger">*</i></label>
                            <input required type="text" class="form-control" name="invoice" placeholder="Enter Invoice">
                        </div>
                        <div class="form-group">
                        <label for="branch_id">branch <i class="text-danger">*</i></label>
                            <select required class="form-control" name="branch_id">
                                    <option value="" >== Select Item ==</option>
                                @foreach ($branch as $b)
                                    <option value="{{$b->id}}">
                                    {{$b->id}} - {{$b->branch_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">Description <i class="text-danger">*</i></label>
                            <textarea required class="form-control" name="description" placeholder="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <input required type="number" class="form-control" name="amount" placeholder="Enter code group">
                        </div>
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input required type="date" class="form-control" name="date" >
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@once

    @push('page_scripts')
        <script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/dataTables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="https://adminlte.io/themes/v3/dist/js/adminlte.min.js"></script>
        <script src="https://adminlte.io/themes/v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        <script src="{{ asset('js/core.js') }}" defer></script>
        <script src="{{ asset('js/production.js') }}" defer></script>
        <script type="text/javascript">
            var i = 0;
            $("#add-btn").click(function(){
            ++i;
            $("#dynamicAddRemove").append('<div id="addfield"><br/><hr width="100%" /><br/><input type="hidden" name="moreFields['+i+'][created_by]" value="{{$user_id}}"><input type="hidden" name="moreFields['+i+'][updated_by]" value="{{$user_id}}"><div class="form-group"><label for="invoice">Invoice <i class="text-danger">*</i></label><input required type="text" class="form-control" name="moreFields['+i+'][invoice]" placeholder="Enter Invoice"></div><div class="form-group"><label for="branch_id">branch <i class="text-danger">*</i></label><select required class="form-control" name="moreFields['+i+'][branch_id]"><option value="" >== Select Item ==</option>@foreach ($branch as $b)<option value="{{$b->id}}">{{$b->id}} - {{$b->branch_name}}</option>@endforeach</select></div><div class="form-group"><label for="description">Description <i class="text-danger">*</i></label><textarea required class="form-control" name="moreFields['+i+'][description]" placeholder="description"></textarea></div><div class="form-group"><label for="amount">Amount</label><input required type="number" class="form-control" name="moreFields['+i+'][amount]" placeholder="Enter code group"></div><div class="form-group"><label for="date">Date</label><input required type="date" class="form-control" name="moreFields['+i+'][date]" ></div><button type="button" class="btn btn-danger remove-tr col-sm-12">Remove Fields</button></div>');
            });
            $(document).on('click', '.remove-tr', function(){  
            $(this).parents('#addfield').remove();
            });  
        </script>
        @endpush

@endonce
