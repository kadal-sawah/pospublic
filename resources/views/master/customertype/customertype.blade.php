@extends('layouts.app')

@once
@push('page_css')
<link rel="stylesheet" href="{{ asset('css/dataTables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">

@endpush
@endonce

@section('content')

    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Tipe Customer
        </h2>

        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <button style="border: none" name="total" class="info-box-icon bg-primary elevation-1"><i class="fas fa-user-cog"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Total</span>
                        <span class="info-box-number" name="all">0
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="trashed" class="info-box-icon bg-info elevation-1"><i class="fas fa-recycle"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Recycle</span>
                        <span class="info-box-number" name="trashed">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Tipe Customer
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-customertype"><i class="fas fa-plus"></i> Tambah</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="delete" class="delete"><i
                                        class="fas fa-trash"></i>
                                    Hapus</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="deleteper"><i
                                        class="fas fa-trash"></i>
                                    Hapus permanent</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive">
                        <table class="table table-bordered table-hovered table-sm" id="table">
                            <thead class="thead-light text-center">
                                <tr>
                                    <th class="text-center" data-orderable="false">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="customCheckbox"
                                                onchange="checkbox_all(this)">
                                            <label for="customCheckbox" class="custom-control-label"></label>
                                        </div>
                                    </th>
                                    <th>Nama Customer</th>
                                    <th>Created At</th>
                                    <th>Update At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-customertype" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="insert-customertype">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Tipe Customer</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="type_name">Tipe Customer</label>
                            <input type="text" class="form-control" name="type_name" placeholder="Tambah Type Customer" required>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between" id="submit-control">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="load-insert">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="edit-customertype" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="update-customertype">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Tipe Customer</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                            <div class="form-group">
                                <label for="type_name">Supplier Type</label>
                                <input type="text" class="form-control" name="type_name" placeholder="Enter name Branch">
                                <input type="hidden" class="form-control" name="id" placeholder="ID Role">
                            </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="load-updated">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@once

    @push('page_scripts')
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/dataTables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="https://adminlte.io/themes/v3/dist/js/adminlte.min.js"></script>
        <script src="https://adminlte.io/themes/v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        {{-- <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script> --}}
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        <script src="{{ asset('js/core.js') }}" defer></script>
        <script src="{{ asset('js/page/customertype/customertype.js') }}" defer></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js') }}"></script>
    @endpush

@endonce
