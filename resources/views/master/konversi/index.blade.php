@extends('layouts.app')

@once
@push('page_css')
<link rel="stylesheet" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
@endpush
@endonce

@section('third_party_scripts')

<script src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>


@endsection

@section('content')
    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Konversi Satuan
        </h2>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Konversi Satuan
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-unit"><i class="fas fa-plus"></i> Add</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm swal-confirm-del" id="deleteAllSelectedRecord">
                                    <i class="fas fa-trash"></i>
                                    Delete Selected</button>
                            </div>
                        </div>
                    </div>
                    @if (session('message'))
                                <div class="alert alert-success alert-dismissible show fade">
                                <div class="alert-body">
                                    <button class="close" data-dismiss="alert">
                                    <span>×</span>
                                    </button>
                                    {{ session('message') }}
                                </div>
                                </div>
                            @endif
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-bordered table-hovered table-striped table-sm" id="my-table">
                            <thead>
                                <tr>
                                    <th width="5%" data-sortable="false">  <input type="checkbox" id="chkCheckAll" /></th>
                                    <th width="5%" class="text-center">No</th>
                                    <th>Nama Satuan</th>
                                    <th>Satuan Baru</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                             <?php $i = 1; ?>
                            @foreach($konversi as $data)
                            <tr id="sid{{ $data->id }}">
                                <td><input type="checkbox" name="ids" class="checkBoxClass" value="{{ $data->id }}" /></td>
                                <td align="center">{{ $i }}</td>
                                <td>{{ $data->name_unit }}</td>
                                <td>{{ $data->satuan_baru }}</td>
                                <td align="center">
                                  <a href="#" data-id="{{ $data->id }}" class="badge badge-success edit-konversi">Edit</a>
                                  <a href="#" data-id="{{ $data->id }}" class="badge badge-danger swal-confirm">
                                  <form action="{{route('delete-konversi',$data->id)}}" id="delete{{$data->id}}" method="POST">
                                  @csrf
                                  @method('delete')
                                  </form>Hapus</a>
                               
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-unit" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Satuan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('insert-konversi') }}" method="POST">
                    @csrf
                <div class="modal-body">
                    <div class="form-group">
                            <label for="exampleFormControlSelect2">Pilih Satuan</label>
                            <br>
                            <select required class="form-control selectpicker" data-live-search="true" name="id_satuan" id="exampleFormControlSelect2">
                              <option value="">-- PILIH SATUAN --</option>
                            @foreach($satuan as $satuan)
                              <option value="{{ $satuan->id }}">{{ $satuan->name_unit }}</option>
                              @endforeach
                            </select>
                        </div>
                    <div class="form-group">
                        <label for="id_role">Satuan Baru</label>
                        <input type="text" class="form-control" name="satuan_baru" placeholder="Satuan Baru" required>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="edit-konversi" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Satuan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{ route('insert-konversi') }}" method="POST" id="form-edit-konversi">
                    @csrf
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary update-konversi">Simpan</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="delete" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div>
        </div>
    </div>
    </div>

@endsection
@once
@push('page_scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{{ asset ('js/page/konversi/konversi.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


<script>
        
</script>

<script>

        @if(Session::has('success'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            }
            toastr.success("{{ Session::get('success') }}");
        @endif

        @if(Session::has('info'))
            toastr.info("{{ Session::get('info') }}");
        @endif
    </script>

@endpush
@endonce

