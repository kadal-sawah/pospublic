@extends('layouts.app')

@once
@push('page_css')
<link rel="stylesheet" href="{{ asset('css/dataTables/dataTables.bootstrap4.min.css') }}">
@endpush
@endonce

@section('content')

    <div class="container-fluid pt-4">
        <h2 class="text-black-10">Recipe Detail
        </h2>

        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <button style="border: none" name="total" class="info-box-icon bg-primary elevation-1"><i class="fas fa-user-cog"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Total</span>
                        <span class="info-box-number" name="all">0
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <button style="border: none" name="trashed" class="info-box-icon bg-info elevation-1"><i class="fas fa-recycle"></i></button>

                    <div class="info-box-content">
                        <span class="info-box-text">Recycle</span>
                        <span class="info-box-number" name="trashed">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Recipe Detail
                        </h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button class="mr-2 float-right btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#add-recipe-detail" ><i class="fas fa-plus"></i> Add</button>
                                <button class="mr-2 float-right btn btn-danger btn-sm" id="delete"><i
                                        class="fas fa-trash"></i>
                                    Delete</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-2">
                        <table class="table table-head-fixed text-nowrap" id="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="customCheckbox"
                                                onchange="checkbox_all(this)">
                                            <label for="customCheckbox" class="custom-control-label"></label>
                                        </div>
                                    </th>
                                    <th>Recipe</th>
                                    <th>Item</th>
                                    <th>Quantity</th>
                                    <th>UOM</th>
                                    <th>Created By</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>

    <!-- insert data -->
    <div class="modal fade" id="add-recipe-detail" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="insert-recipe-detail">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Recipe</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipe_id">Recipe <i class="text-danger">*</i></label>
                            <select required class="form-control" name="recipe_id">
                                    <option value="" >== Select Item ==</option>
                                @foreach ($recipe as $r)
                                    <option value="{{$r->id}}">
                                    {{$r->id}} - {{$r->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="item_id">Item <i class="text-danger">*</i></label>
                            <select required class="form-control" name="item_id">
                                    <option value="" >== Select Item ==</option>
                                @foreach ($item as $i)
                                    <option value="{{$i->id}}">
                                    {{$i->id}} - {{$i->item_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="uom_code">UOM <i class="text-danger">*</i></label>
                            <select required class="form-control" name="uom_code">
                                    <option value="" >== Select Item ==</option>
                                @foreach ($uom as $u)
                                    <option value="{{$u->id}}">
                                    {{$u->id}} - {{$u->name_unit}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="qty">Quantity <i class="text-danger">*</i></label>
                            <input required type="number" class="form-control" name="qty" placeholder="Enter Quantity">
                        </div>
                        <div class="form-group">
                            <label for="memo">Memo </label>
                            <textarea required class="form-control" name="memo" placeholder="Enter Memo"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- update data -->
    <div class="modal fade" id="edit-recipe-detail" aria-modal="true" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" id="update-recipe-detail">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit recipe</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" name="id" placeholder="ID recipe">
                        <div class="form-group">
                            <label for="recipe_id">Recipe <i class="text-danger">*</i></label>
                            <select required class="form-control" name="recipe_id">
                                    <option value="" >== Select Item ==</option>
                                @foreach ($recipe as $r)
                                    <option value="{{$r->id}}">
                                    {{$r->id}} - {{$r->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="item_id">Item <i class="text-danger">*</i></label>
                            <select required class="form-control" name="item_id">
                                    <option value="" >== Select Item ==</option>
                                @foreach ($item as $i)
                                    <option value="{{$i->id}}">
                                    {{$i->id}} - {{$i->item_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="uom_code">UOM <i class="text-danger">*</i></label>
                            <select required class="form-control" name="uom_code">
                                    <option value="" >== Select Item ==</option>
                                @foreach ($uom as $u)
                                    <option value="{{$u->id}}">
                                    {{$u->id}} - {{$u->name_unit}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="qty">Quantity <i class="text-danger">*</i></label>
                            <input required type="number" class="form-control" name="qty" placeholder="Enter Quantity">
                        </div>
                        <div class="form-group">
                            <label for="memo">Memo </label>
                            <textarea required class="form-control" name="memo" placeholder="Enter Memo"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection


@once

    @push('page_scripts')
        <script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('js/dataTables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="https://adminlte.io/themes/v3/dist/js/adminlte.min.js"></script>
        <script src="https://adminlte.io/themes/v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
        <script src="{{ asset('js/core.js') }}" defer></script>
        <script src="{{ asset('js/recipedetail.js') }}" defer></script>
    @endpush

@endonce
