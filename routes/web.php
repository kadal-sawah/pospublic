<?php

use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('master/role', 'RoleController@index')->name('role'); *pindah ke route group (master)

// Group Route Master
Route::group(['prefix' => 'master', 'middleware' => 'auth'],function () {
    
    // Group Router API User
    Route::prefix('user')->group(function () {
        Route::get('/user', 'UserController@index')->name('user');
        Route::get('/get', 'UserController@get')->name('user_get');
        Route::post('/insert', 'UserController@insert')->name('user_insert');
        Route::post('/update', 'UserController@update')->name('user_update');
        Route::get('/get/{id}', 'UserController@getById')->name('user_get_by_id');
        Route::delete('/delete', 'UserController@delete')->name('user_delete');
        Route::get('/all', 'UserController@all')->name('user_all');
        Route::put('/change', 'UserController@change')->name('user_change');
    });


//company profile
    Route::prefix('company-profile')->group(function(){
        Route::get('/', 'CompanyProfileController@index')->name('company-profile');
        Route::post('/add', 'CompanyProfileController@store')->name('add-company-profile');
        Route::post('/update', 'CompanyProfileController@update')->name('update-company-profile'); 
        Route::get('/get', 'CompanyProfileController@get')->name('company-profile-get');
        Route::get('/get/{id}', 'CompanyProfileController@getById')->name('company-profile-get-by-id');
        Route::get('/all', 'CompanyProfileController@all')->name('company-profile-all');
        Route::put('/change', 'CompanyProfileController@change')->name('company-profile-change');
        Route::delete('/delete', 'CompanyProfileController@delete')->name('company-profile-delete');
    });

// Group Route Master
Route::group(['prefix' => 'master', 'middleware' => 'auth'],function () {
    // Group Router API Role
    Route::prefix('role')->group(function () {
        Route::get('/', 'RoleController@index')->name('role');
        Route::post('/insert', 'RoleController@insert')->name('branch_insert');
        Route::post('/update', 'RoleController@update')->name('role_update');
        Route::get('/get', 'RoleController@get')->name('role_get');
        Route::get('/get/{id}', 'RoleController@getById')->name('role_get_by_id');
        Route::delete('/delete', 'RoleController@delete')->name('role_delete');
        Route::get('/all', 'RoleController@all')->name('role_all');
        Route::put('/change', 'RoleController@change')->name('role_change');
    });

    // Group Router API Branch
    Route::prefix('branch')->group(function () {    
        Route::get('/', 'BranchController@index')->name('branch');
        Route::get('/get', 'BranchController@get')->name('branch_get');
        Route::post('/insert', 'BranchController@insert')->name('branch_insert');
        Route::post('/update', 'BranchController@update')->name('branch_update');
        Route::get('/get/{id}', 'BranchController@getById')->name('branch_get_by_id');
        Route::delete('/delete', 'BranchController@delete')->name('branch_delete');
        Route::get('/all', 'BranchController@all')->name('branch_all');
        Route::put('/change', 'BranchController@change')->name('branch_change');
    });

    // Group Router API Item
    Route::prefix('item')->group(function(){   
        Route::get('/', 'ItemController@index')->name('item');
        Route::post('/add', 'ItemController@store')->name('add-item');
        Route::post('/update', 'ItemController@update')->name('update-item');
        Route::get('/get', 'ItemController@get')->name('item-get');
        Route::get('/get/{id}', 'ItemController@getById')->name('item-get-by-id');
        Route::get('/all', 'ItemController@all')->name('item-all');
        Route::put('/change', 'ItemController@change')->name('item-change');
        Route::delete('/delete', 'ItemController@delete')->name('item-delete');
        //item group (kelompok barang)
        Route::prefix('group')->group(function(){
            Route::get('/', 'ItemgroupController@index')->name('item-group');
            Route::post('/add', 'ItemgroupController@store')->name('add-item-group');
            Route::post('/update', 'ItemgroupController@update')->name('update-item-group'); 
            Route::get('/get', 'ItemgroupController@get')->name('item-group-get');
            Route::get('/get/{id}', 'ItemgroupController@getById')->name('item-group-get-by-id');
            Route::get('/all', 'ItemgroupController@all')->name('item-group-all');
            Route::put('/change', 'ItemgroupController@change')->name('item-group-change');
            Route::delete('/delete', 'ItemgroupController@delete')->name('item-group-delete');
        });
        //item price (harga item)
        Route::prefix('price')->group(function(){
            Route::get('/', 'ItemPriceController@index')->name('item-price');
            Route::post('/add', 'ItemPriceController@store')->name('add-item-price');
            Route::post('/update', 'ItemPriceController@update')->name('update-item-price'); 
            Route::get('/get', 'ItemPriceController@get')->name('item-group-get');
            Route::get('/get/{id}', 'ItemPriceController@getById')->name('item-price-get-by-id');
            Route::get('/all', 'ItemPriceController@all')->name('item-price-all');
            Route::put('/change', 'ItemPriceController@change')->name('item-price-change');
            Route::delete('/delete', 'ItemPriceController@delete')->name('item-price-delete');
        });
        //Discount (diskon)
        Route::prefix('discount')->group(function(){
            Route::get('/', 'DiscountController@index')->name('discount');
            Route::post('/add', 'DiscountController@store')->name('add-discount');
            Route::post('/update', 'DiscountController@update')->name('update-discount'); 
            Route::get('/get', 'DiscountController@get')->name('discount-get');
            Route::get('/get/{id}', 'DiscountController@getById')->name('discount-get-by-id');
            Route::get('/all', 'DiscountController@all')->name('discount-all');
            Route::put('/change', 'DiscountController@change')->name('discount-change');
            Route::delete('/delete', 'DiscountController@delete')->name('discount-delete');
        });
        //Discount type (tipe diskon)
        Route::prefix('discount/type')->group(function(){
            Route::get('/', 'DiscountTypeController@index')->name('discount-type');
            Route::post('/add', 'DiscountTypeController@store')->name('add-discount-type');
            Route::post('/update', 'DiscountTypeController@update')->name('update-discount-type'); 
            Route::get('/get', 'DiscountTypeController@get')->name('item-group-get');
            Route::get('/get/{id}', 'DiscountTypeController@getById')->name('discount-type-get-by-id');
            Route::get('/all', 'DiscountTypeController@all')->name('item-group-all');
            Route::put('/change', 'DiscountTypeController@change')->name('discount-type-change');
            Route::delete('/delete', 'DiscountTypeController@delete')->name('discount-type-delete');
        });
        // Item Warehouse
        Route::prefix('warehouse')->group(function () {
            Route::get('/', 'ItemWarehouseController@index')->name('item-warehouse');
            Route::get('/all', 'ItemWarehouseController@all')->name('item-warehouse-all');
            Route::post('/add', 'ItemWarehouseController@store')->name('add-item-warehouse');
            Route::post('/update', 'ItemWarehouseController@update')->name('update-item-warehouse'); 
            Route::get('/get', 'ItemWarehouseController@get')->name('item-group-get');
            Route::get('/get/{id}', 'ItemWarehouseController@getById')->name('item-warehouse-get-by-id');
            Route::put('/change', 'ItemWarehouseController@change')->name('item-warehouse-change');
            Route::delete('/delete', 'ItemWarehouseController@delete')->name('item-warehouse-delete');
        });
    
    });

    // Satuan
    Route::prefix('unit')->group(function(){
        Route::get('/', 'SatuanController@index')->name('unit');
        Route::get('/get', 'SatuanController@satuanGet')->name('satuan-get');
        Route::post('/insert', 'SatuanController@satuanInsert')->name('add-unit');
        Route::post('/update', 'SatuanController@update')->name('satuan-update');
        Route::get('/get/{id}', 'SatuanController@getById')->name('satuan-get_by_id');
        Route::delete('/delete', 'SatuanController@delete')->name('satuan-delete');
        Route::get('/all', 'SatuanController@all')->name('satuan-all');
        Route::put('/change', 'SatuanController@change')->name('satuan-change');
    });
    
    // Konversi Satuan
    Route::prefix('konversi')->group(function(){
        Route::get('/', 'KonversiSatuanController@index')->name('konversi');
        Route::get('/get', 'KonversiSatuanController@konversiGet')->name('konversi-get');
        Route::post('/insert', 'KonversiSatuanController@konversiInsert')->name('konversi-add');
        Route::get('/edit/{id}','KonversiSatuanController@edit')->name('konversi-edit');
        Route::post('/update', 'KonversiSatuanController@update')->name('konversi-update');
        Route::get('/get/{id}', 'KonversiSatuanController@getById')->name('konversi-get_by_id');
        Route::delete('/delete', 'KonversiSatuanController@delete')->name('konversi-delete');
        Route::get('/all', 'konversiSatuanController@all')->name('konversi-all');
    });

    // Group Router API Merk
    Route::prefix('merk')->group(function () {
        Route::get('/', 'MerkController@index')->name('merk');
        Route::post('/storeMerk', 'MerkController@store')->name('storeMerk');
        Route::delete('/destroyMerk/{id}','MerkController@destroy' )->name('destroyMerk');
        Route::patch('/statusMerk/{id}', 'MerkController@updateState')->name('statusMerk');
    });

});

// Master Data Route
Route::group(['prefix' => 'tables', 'middleware' => 'auth','cors'],function () {
    Route::prefix('merk')->group(function(){
        Route::get('/','Tables\MerkDatatables@anyData');
    });
});



// Master API Route
// Route::prefix('api')->middleware('cors')->group(function (){

//     Route::prefix('merk')->group(function(){
//             Route::get('/', 'Api\MerkApi@index');
//             // Route::get('/', function(){
//                 //     return 'ok';
//             // });
//     });

    // Group Router API Production
    Route::prefix('production')->group(function () {
        Route::get('/', 'ProductionController@index')->name('production');
        Route::post('/add', 'ProductionController@store')->name('producton-add');
        Route::post('/update', 'ProductionController@update')->name('production-update');
        Route::get('/get', 'ProductionController@get')->name('production-get');
        Route::get('/get/{id}', 'ProductionController@getById')->name('production-get-by-id');
        Route::delete('/delete', 'ProductionController@delete')->name('production-delete');
        Route::get('/all', 'ProductionController@all')->name('production-all');
        Route::put('/change', 'ProductionController@change')->name('production-status-change');

        // Group Router API Production Detail
        Route::prefix('detail')->group(function () {
            Route::get('/', 'ProductionDetailController@index')->name('production-detail');
            Route::post('/add', 'ProductionDetailController@store')->name('producton-detail-add');
            Route::post('/update', 'ProductionDetailController@update')->name('production-detail-update');
            Route::get('/get', 'ProductionDetailController@get')->name('production-detail-get');
            Route::get('/get/{id}', 'ProductionDetailController@getById')->name('production-detail-get-by-id');
            Route::delete('/delete', 'ProductionDetailController@delete')->name('production-detail-delete');
            Route::get('/all', 'ProductionDetailController@all')->name('production-detail-all');
            Route::put('/change', 'ProductionDetailController@change')->name('production-detail-status-change');
        });
    });

    // Group Router API Recipe
    Route::prefix('recipe')->group(function () {
            Route::get('/', 'RecipeController@index')->name('recipe');
            Route::post('/add', 'RecipeController@store')->name('recipe-add');
            Route::post('/update', 'RecipeController@update')->name('recipe-update');
            Route::get('/get', 'RecipeController@get')->name('recipe-get');
            Route::get('/get/{id}', 'RecipeController@getById')->name('recipe-get-by-id');
            Route::delete('/delete', 'RecipeController@delete')->name('recipe-delete');
            Route::get('/all', 'RecipeController@all')->name('recipe-all');
            Route::put('/change', 'RecipeController@change')->name('recipe-status-change');

        // Group Router API Recipe Detail
        Route::prefix('detail')->group(function () {
                Route::get('/', 'RecipeDetailController@index')->name('recipe-detail');
                Route::post('/add', 'RecipeDetailController@store')->name('recipe-detail-add');
                Route::post('/update', 'RecipeDetailController@update')->name('recipe-detail-update');
                Route::get('/get', 'RecipeDetailController@get')->name('recipe-detail-get');
                Route::get('/get/{id}', 'RecipeDetailController@getById')->name('recipe-detail-get-by-id');
                Route::delete('/delete', 'RecipeDetailController@delete')->name('recipe-detail-delete');
                Route::get('/all', 'RecipeDetailController@all')->name('recipe-detail-all');
                Route::put('/change', 'RecipeDetailController@change')->name('recipe-detail-status-change');
            });
        });

    
        // Group Router API Role
    Route::prefix('role')->group(function () {
        Route::get('/', 'RoleController@index')->name('role');
        Route::post('/insert', 'RoleController@insert')->name('branch_insert');
        Route::post('/update', 'RoleController@update')->name('role_update');
        Route::get('/get', 'RoleController@get')->name('role_get');
        Route::get('/get/{id}', 'RoleController@getById')->name('role_get_by_id');
        Route::delete('/delete', 'RoleController@delete')->name('role_delete');
        Route::get('/all', 'RoleController@all')->name('role_all');
        Route::put('/change', 'RoleController@change')->name('role_change');
    });

    // Group Router API Branch
    Route::prefix('branch')->group(function () {    
        Route::get('/', 'BranchController@index')->name('branch');
        Route::get('/get', 'BranchController@get')->name('branch_get');
        Route::post('/insert', 'BranchController@insert')->name('branch_insert');
        Route::post('/update', 'BranchController@update')->name('branch_update');
        Route::get('/get/{id}', 'BranchController@getById')->name('branch_get_by_id');
        Route::delete('/delete', 'BranchController@delete')->name('branch_delete');
        Route::get('/all', 'BranchController@all')->name('branch_all');
        Route::put('/change', 'BranchController@change')->name('branch_change');
    });

    // Group Router API Item
    Route::prefix('item')->group(function(){   
        Route::get('/', 'ItemController@index')->name('item');
        Route::post('/add-item', 'ItemController@store')->name('add-item');
        Route::post('/{id}', 'ItemController@update')->name('update-item');
        Route::get('/{id}/edit', 'ItemController@edit')->name('edit-item');
        Route::get('/delete/{id}','ItemController@destroy')->name('item-delete');
        Route::delete('/selected-item','ItemController@deleteCheckboxItem')->name('item-deleteSelected');
        //item group (kelompok barang)
        Route::get('/group', 'ItemgroupController@index')->name('item-group');
        Route::post('/group/add', 'ItemgroupController@store')->name('add-item-group');
        Route::get('/group/{id}/edit', 'ItemgroupController@edit')->name('edit-item-group');
        Route::patch('/group/{id}/update', 'ItemgroupController@update')->name('update-item-group');
        Route::get('/group/delete/{id}','ItemgroupController@destroy')->name('item-group-delete');
        Route::delete('/group/selected','ItemgroupController@deleteCheckboxItem')->name('item-group-deleteSelected');
    });

    // Satuan
    Route::prefix('unit')->group(function(){
        Route::get('/', 'SatuanController@index')->name('unit');
        Route::get('/get', 'SatuanController@satuanGet')->name('satuan-get');
        Route::post('/insert', 'SatuanController@satuanInsert')->name('add-unit');
        Route::post('/update', 'SatuanController@update')->name('satuan-update');
        Route::get('/get/{id}', 'SatuanController@getById')->name('satuan-get_by_id');
        Route::delete('/delete', 'SatuanController@delete')->name('satuan-delete');
        Route::get('/all', 'SatuanController@all')->name('satuan-all');
        Route::put('/change', 'SatuanController@change')->name('satuan-change');
    });
    
    // Konversi Satuan
    Route::prefix('konversi')->group(function(){
        Route::get('/', 'KonversiSatuanController@index')->name('konversi');
        Route::get('/get', 'KonversiSatuanController@konversiGet')->name('konversi-get');
        Route::post('/insert', 'KonversiSatuanController@konversiInsert')->name('konversi-add');
        Route::get('/edit/{id}','KonversiSatuanController@edit')->name('konversi-edit');
        Route::post('/update', 'KonversiSatuanController@update')->name('konversi-update');
        Route::get('/get/{id}', 'KonversiSatuanController@getById')->name('konversi-get_by_id');
        Route::delete('/delete', 'KonversiSatuanController@delete')->name('konversi-delete');
        Route::get('/all', 'konversiSatuanController@all')->name('konversi-all');
    });

    // Group Router API User
    Route::prefix('user')->group(function () {
        Route::get('/user', 'UserController@index')->name('user');
        Route::get('/get', 'UserController@get')->name('user_get');
        Route::post('/insert', 'UserController@insert')->name('user_insert');
        Route::post('/update', 'UserController@update')->name('user_update');
        Route::get('/get/{id}', 'UserController@getById')->name('user_get_by_id');
        Route::delete('/delete', 'UserController@delete')->name('user_delete');
        Route::get('/all', 'UserController@all')->name('user_all');
        Route::put('/change', 'UserController@change')->name('user_change');
    });

    // Group Router API SupplierType
    Route::prefix('suppliertype')->group(function () {    
        Route::get('/', 'SupplierTypeController@index')->name('suppliertype');
        Route::get('/get', 'SupplierTypeController@suppliertypeGet')->name('suppliertype-get');
        Route::post('/insert', 'SupplierTypeController@suppliertypeInsert')->name('suppliertype-insert');
        Route::post('/update', 'SupplierTypeController@update')->name('suppliertype-update');
        Route::get('/get/{id}', 'SupplierTypeController@getById')->name('suppliertype-get_by_id');
        Route::delete('/delete', 'SupplierTypeController@delete')->name('suppliertype-delete');
        Route::get('/all', 'SupplierTypeController@all')->name('suppliertype-all');
        Route::put('/change', 'SupplierTypeController@change')->name('suppliertype-change');
    });

    // Group Route API Supplier
    Route::prefix('supplier')->group(function () { 
        Route::get('/', 'SupplierController@index')->name('supplier');
        Route::get('/get', 'SupplierController@supplierGet')->name('supplier-get');
        Route::post('/insert', 'SupplierController@supplierInsert')->name('supplier-add');
        Route::get('/edit/{id}','SupplierController@edit')->name('supplier-edit');
        Route::post('/update', 'SupplierController@update')->name('supplier-update');
        Route::get('/get/{id}', 'SupplierController@getById')->name('supplier-get_by_id');
        Route::delete('/delete', 'SupplierController@delete')->name('supplier-delete');
        Route::get('/all', 'SupplierController@all')->name('supplier-all');
        Route::put('/change', 'SupplierController@change')->name('supplier-change');
    });

    // Group Rote API Customer_type
    Route::prefix('customertype')->group(function(){
        Route::get('/', 'CustomerTypeController@index')->name('customertype');
        Route::get('/get','CustomerTypeController@customertypeGet')->name('customertype-get');
        Route::post('/insert','CustomerTypeController@customertypeInsert')->name('customertype-add');
        Route::get('/edit/{id}','CustomerTypeController@edit')->name('customertype-edit');
        Route::post('/update','CustomerTypeController@update')->name('customertype-update');
        Route::get('/get/{id}','CustomerTypeController@getById')->name('customertype-get_by_id');
        Route::delete('/delete','CustomerTypeController@delete')->name('customertype-delete');
        Route::get('/all','CustomerTypeController@all')->name('customertype-all');
    });

    // Group Rote API Division
    Route::prefix('division')->group(function(){
        Route::get('/', 'DivisionController@index')->name('division');
        Route::get('/get','DivisionController@divisionGet')->name('division-get');
        Route::post('/insert','DivisionController@divisionInsert')->name('division-add');
        Route::get('/edit/{id}','DivisionController@edit')->name('division-edit');
        Route::post('/update','DivisionController@update')->name('division-update');
        Route::get('/get/{id}','DivisionController@getById')->name('division-get_by_id');
        Route::delete('/delete','DivisionController@delete')->name('division-delete');
        Route::get('/all','DivisionController@all')->name('division-all');
    });

    // Group Rote API Shift
    Route::prefix('shift')->group(function(){
        Route::get('/', 'ShiftController@index')->name('shift');
        Route::get('/get','ShiftController@shiftGet')->name('shift-get');
        Route::post('/insert','ShiftController@shiftInsert')->name('shift-add');
        Route::get('/edit/{id}','ShiftController@edit')->name('shift-edit');
        Route::post('/update','ShiftController@update')->name('shift-update');
        Route::get('/get/{id}','ShiftController@getById')->name('shift-get_by_id');
        Route::delete('/delete','ShiftController@delete')->name('shift-delete');
        Route::get('/all','ShiftController@all')->name('shift-all');
    });

    // Group Router API Color
    Route::prefix('color')->group(function () {    
        Route::get('/', 'ColorController@index')->name('color');
        Route::get('/get', 'ColorController@colorGet')->name('color-get');
        Route::post('/insert', 'ColorController@colorInsert')->name('color-insert');
        Route::post('/update', 'ColorController@update')->name('color-update');
        Route::get('/get/{id}', 'ColorController@getById')->name('color-get_by_id');
        Route::delete('/delete', 'ColorController@delete')->name('color-delete');
        Route::get('/all', 'ColorController@all')->name('color-all');
        Route::put('/change', 'ColorController@change')->name('color-change');
    });

    // Group Rote API Table
    Route::prefix('table')->group(function(){
        Route::get('/', 'TableController@index')->name('table');
        Route::get('/get','TableController@tableGet')->name('table-get');
        Route::post('/insert','TableController@tableInsert')->name('table-add');
        Route::get('/edit/{id}','TableController@edit')->name('table-edit');
        Route::post('/update','TableController@update')->name('table-update');
        Route::get('/get/{id}','TableController@getById')->name('table-get_by_id');
        Route::delete('/delete','TableController@delete')->name('table-delete');
        Route::get('/all','TableController@all')->name('table-all');
    });

    // Group Rote API Majors
    Route::prefix('majors')->group(function(){
        Route::get('/', 'MajorsController@index')->name('majors');
        Route::get('/get','MajorsController@majorsGet')->name('majors-get');
        Route::post('/insert','MajorsController@majorsInsert')->name('majors-add');
        Route::get('/edit/{id}','MajorsController@edit')->name('majors-edit');
        Route::post('/update','MajorsController@update')->name('majors-update');
        Route::get('/get/{id}','MajorsController@getById')->name('majors-get_by_id');
        Route::delete('/delete','MajorsController@delete')->name('majors-delete');
        Route::get('/all','MajorsController@all')->name('majors-all');
    });

    // Group Rote API TypePayment
    Route::prefix('typepayment')->group(function(){
        Route::get('/', 'TypePaymentController@index')->name('typepayment');
        Route::get('/get','TypePaymentController@typepaymentGet')->name('typepayment-get');
        Route::post('/insert','TypePaymentController@typepaymentInsert')->name('typepayment-add');
        Route::get('/edit/{id}','TypePaymentController@edit')->name('typepayment-edit');
        Route::post('/update','TypePaymentController@update')->name('typepayment-update');
        Route::get('/get/{id}','TypepaymentController@getById')->name('typepayment-get_by_id');
        Route::delete('/delete','TypePaymentController@delete')->name('typepayment-delete');
        Route::get('/all','TypePaymentController@all')->name('typepayment-all');
    });

    // Group Rote API Clasification
    Route::prefix('clasification')->group(function(){
        Route::get('/', 'ClasificationController@index')->name('clasification');
        Route::get('/get','ClasificationController@clasificationGet')->name('clasification-get');
        Route::post('/insert','ClasificationController@clasificationInsert')->name('clasification-add');
        Route::get('/edit/{id}','ClasificationController@edit')->name('clasification-edit');
        Route::post('/update','ClasificationController@update')->name('clasification-update');
        Route::get('/get/{id}','ClasificationController@getById')->name('clasification-get_by_id');
        Route::delete('/delete','ClasificationController@delete')->name('clasification-delete');
        Route::get('/all','ClasificationController@all')->name('clasification-all');
    });

    // Group Rote API Customer
    Route::prefix('customer')->group(function(){
        Route::get('/', 'CustomerController@index')->name('customer');
        Route::get('/get','CustomerController@customerGet')->name('customer-get');
        Route::match(['get','post'],'/insert','CustomerController@customerInsert')->name('customer-add');
        Route::match(['get','post'],'/{id}/edit','CustomerController@edit')->name('customer-edit');
        Route::post('/update','CustomerController@update')->name('customer-update');
        Route::get('/get/{id}','CustomerController@getById')->name('customer-get_by_id');
        Route::delete('/delete','CustomerController@delete')->name('customer-delete');
        Route::get('/all','CustomerController@all')->name('customer-all');
        Route::put('/change', 'CustomerController@change')->name('customer_change');
        Route::get('provinces', 'CustomerController@provinces')->name('provinces');
        Route::get('cities', 'CustomerController@cities')->name('cities');
        Route::get('districts', 'CustomerController@districts')->name('districts');
        Route::get('villages', 'CustomerController@villages')->name('villages');
    });

    // Group Route Api Bincard
    Route::prefix('bincard')->group(function(){
        Route::resource('/crud','BincardController');
        Route::put('/change', 'BincardController@change')->name('bincard-change');
        Route::get('/get','BincardController@customerGet')->name('bincard-get');
        Route::get('/all','BincardController@all')->name('bincard-all');
        Route::delete('/delete', 'BincardController@delete')->name('delete');
    });

});
