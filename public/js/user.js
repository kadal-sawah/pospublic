// $.noConflict();
const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "name", name: "name" },
    { data: "email", name: "email" },
    { data: "status", name: "status" },
    { data: "created_at", name: "created_at" },
    // { data: "created_at", name: "created_at" },
    { data: "btn", name: "btn" }
];

$(function() {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/user/get" });

    $("#delete").on("click", function() {
        $.ajax({
            url: "/master/user/delete",
            data: {
                id: value_checkbox
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("change", ".status", function() {
        $.ajax({
            url: "/master/user/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('button[name="total"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/user/get"});
    });

    $('button[name="trashed"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/user/get", parm:{parm:'trashed'}});
    });
    $('button[name="active"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/user/get", parm:{parm:'status', value:1}});
    });
    $('button[name="inactive"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/user/get", parm:{parm:'status', value:0}});
    });
});

$("#insert-user").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/user/insert",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("table");
            $("#add-user").modal("hide");
            // getAllData();
        },
        error: err => console.log(err)
    });
});

$("#update-user").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/user/update",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("table");
            getAllData();
            $("#edit-user").modal("hide");
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#edit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/user/get/" + $(this).data("id"),
        type: "get",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            $("#edit-user").modal("show");
            Object.keys(res.data).map(key => {
                $(`#edit-user .form-control[name="${key}"]`).val(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#delete", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/user/delete",
        data: {
            id: $(this).data("id")
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("table");
            getAllData();
        },
        error: err => console.log(err)
    });
});


function getAllData() {
    $.ajax({
        url: "/master/user/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}
