const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "invoice", name: "invoice" },
    { data: "branch.branch_name", name: "branch.branch_name" },
    { data: "amount", name: "amount" },
    { data: "date", name: "date" },
    {
        data: "status",
        name: "status",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "user.name", name: "user.name" },
    { data: "user_update.name", name: "user_update.name" },
    { data: "created_at", name: "created_at" },
    {
        data: "btn",
        name: "btn",
        orderable: false,
        ordering: false,
        sortable: false,
        searchable: false
    }
];

$(function() {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/production/get" });

    $("#delete").on("click", function() {
        $.ajax({
            url: "/master/production/delete",
            data: {
                id: value_checkbox
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                Table({
                    table: "#table",
                    data: data,
                    url: "/master/production/get"
                });
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("change", ".status", function() {
        $.ajax({
            url: "/master/production/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('button[name="total"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/production/get" });
    });
    $('button[name="trashed"]').on("click", function() {
        Table({
            table: "#table",
            data: data,
            url: "/master/production/get",
            parm: { parm: "trashed" }
        });
    });
    $('button[name="active"]').on("click", function() {
        Table({
            table: "#table",
            data: data,
            url: "/master/production/get",
            parm: { parm: "status", value: 1 }
        });
    });
    $('button[name="inactive"]').on("click", function() {
        Table({
            table: "#table",
            data: data,
            url: "/master/production/get",
            parm: { parm: "status", value: 0 }
        });
    });
});

$("#insert-production").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/add",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        beforeSend: function() {
            $("#load").prop("disabled", true);
            $("#load").html("Loading...");
        },
        success: res => {
            location.reload(true);
        },
        error: err => console.log(err)
    });
});

$("#update-production").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/update",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("table");
            getAllData();
            $("#edit-production").modal("hide");
            $(".modal-backdrop").remove();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#edit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/get/" + $(this).data("id"),
        type: "get",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            $("#edit-production").modal("show");
            Object.keys(res.data).map(key => {
                $(`#edit-production .form-control[name="${key}"]`).val(
                    res.data[key]
                );
            });
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#delete", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/delete",
        data: {
            id: $(this).data("id")
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({
                table: "#table",
                data: data,
                url: "/master/production/get"
            });
            //RefreshTable("item_group");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#restore", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/delete",
        data: {
            id: $(this).data("id"),
            parm: "restore"
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({
                table: "#table",
                data: data,
                url: "/master/production/get",
                parm: { parm: "trashed" }
            });
            //RefreshTable("role");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#deletePermanent", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/delete",
        data: {
            id: $(this).data("id"),
            parm: "deletePermanent"
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({
                table: "#table",
                data: data,
                url: "/master/production/get",
                parm: { parm: "trashed" }
            });
            getAllData();
        },
        error: err => console.log(err)
    });
});

function getAllData() {
    $.ajax({
        url: "/master/production/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}
