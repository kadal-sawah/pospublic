const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "item.item_name", name: "item.item_name" },
    { data: "price", name: "price" },
    { data: "qty", name: "qty" },
    { data: "amount_price", name: "amount_price" },
    { data: "user.name", name: "user.name" },
    { data: "created_at", name: "created_at" },
    { data: "btn", name: "btn",
        orderable: false,
        ordering: false,
        sortable: false,
        searchable: false 
    }
];

$(function() {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/production/detail/get" });

    $("#delete").on("click", function() {
        $.ajax({
            url: "/master/production/detail/delete",
            data: {
                id: value_checkbox
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                Table({ table: "#table", data: data, url: "/master/production/detail/get"});
                //RefreshTable("role");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("change", ".status", function() {
        $.ajax({
            url: "/master/production/detail/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('button[name="total"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/production/detail/get"});
    });
    $('button[name="trashed"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/production/detail/get", parm:{parm:'trashed'}});
    });
});

$("#insert-production-detail").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/detail/add",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        beforeSend: function(){
            $('#load').html("Loading...");
            $('#load').prop("disabled", true);
        },
        success: res => {
            location.reload(true);
        },
        // success: res => {
        //     RefreshTable("table");
        //     getAllData();
        //     $('#add-production-detail').modal('hide');
        //     $('.modal-backdrop').remove();
        // },
        error: err => console.log(err)
    });
});

$("#update-production-detail").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/detail/update",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        beforeSend: function(){
            $('#updating').html("Updating...");
            $('#updating').prop("disabled", true);
        },
        success: res => {
            RefreshTable("table");
            getAllData();
            $('#edit-production-detail').modal('hide');
            $('.modal-backdrop').remove();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#edit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/detail/get/" + $(this).data("id"),
        type: "get",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            $("#edit-production-detail").modal("show");
            Object.keys(res.data).map(key => {
                $(`#edit-production-detail .form-control[name="${key}"]`).val(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#delete", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/detail/delete",
        data: {
            id: $(this).data("id")
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/production/detail/get"});
            //RefreshTable("item_group");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#restore", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/detail/delete",
        data: {
            id: $(this).data("id"),
            parm : 'restore'
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/production/detail/get", parm:{parm:'trashed'}});
            //RefreshTable("role");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#deletePermanent", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/production/detail/delete",
        data: {
            id: $(this).data("id"),
            parm : 'deletePermanent'
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/production/detail/get", parm:{parm:'trashed'}});
            getAllData();
        },
        error: err => console.log(err)
    });
});

function getAllData() {
    $.ajax({
        url: "/master/production/detail/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}
