const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "name_unit", name: "name_unit" },
    { data: "singkatan", name: "singkatan" },
    {
        data: "status",
        name: "status",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: 'created_at', name: 'created_at' },
    { data: 'updated_at', name: 'updated_at' },
    { data: "btn", name: "btn" },
];

$(function () {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/unit/get" });

    $('table').DataTable();

    $("#delete").on("click", function () {
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: "/master/unit/delete",
                        data: {
                            id: value_checkbox,
                            parm: "withTrashed"
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });

    $("#table").on("change", ".status", function () {
        $.ajax({
            url: "/master/unit/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('button[name="total"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/unit/get" });
    });

    $('button[name="trashed"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/unit/get", parm: { parm: 'trashed' } });
    });
    $('button[name="active"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/unit/get", parm: { parm: 'status', value: 1 } });
    });
    $('button[name="inactive"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/unit/get", parm: { parm: 'status', value: 0 } });
    });

    $("#insert-unit").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/master/unit/insert",
            data: new FormData(this),
            processData: false,
            contentType: false,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            beforeSend: function () {
                $('#insert-unit').trigger("reset");
                $('#load-insert').html("Loading...");
                $('#load-insert').prop("disabled", true);
            },
            success: res => {
                $('#load-insert').prop("disabled", false);
                $('#load-insert').html("Save changes");
                RefreshTable("table");
                getAllData();
                $("#add-unit").modal('hide');
                $('.modal-backdrop').remove();
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "2000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr.success('Data Berhasil Di Tambahkan !', 'Konversi Satuan')
            },
            error: err => console.log(err)
        });
    });

    $("#update-unit").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/master/unit/update",
            data: new FormData(this),
            processData: false,
            contentType: false,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            beforeSend: function () {
                $('#update-unit').trigger("reset");
                $('#load-updated').html("Loading...");
                $('#load-updated').prop("disabled", true);
            },
            success: res => {
                $('#load-updated').prop("disabled", false);
                $('#load-updated').html("Save changes");
                RefreshTable("table");
                getAllData();
                $("#edit-unit").modal("hide");
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "2000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr.success('Data Berhasil Di Edit !', 'Konversi Satuan');
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#edit", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/master/unit/get/" + $(this).data("id"),
            type: "get",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                $("#edit-unit").modal("show");
                Object.keys(res.data).map(key => {
                    $(`#edit-unit .form-control[name="${key}"]`).val(res.data[key]);
                });
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#delete", function (e) {
        e.preventDefault();
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: "/master/unit/delete",
                        data: {
                            id: $(this).data("id")
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });

    $("#table").on("click", "#restore", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/master/unit/delete",
            data: {
                id: $(this).data("id"),
                parm: 'restore'
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#deletePermanent", function (e) {
        e.preventDefault();
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: `/master/unit/delete`,
                        data: {
                            id: $(this).data("id"),
                            parm: 'deletePermanent'
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });
})

function getAllData() {
    $.ajax({
        url: "/master/unit/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}