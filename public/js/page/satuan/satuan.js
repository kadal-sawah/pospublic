$.noConflict();

$(function () {
    console.log('ok');
    dataTable(false);

    //EDIT & UPDATE SATUAN
    $('.btn-edit').on('click', function () {
        // console.log($(this).data('id'))

        let id = $(this).data('id')
        $.ajax({
            url: `/master/unit/edit/${id}`,
            method: "GET",
            success: function (data) {
                // console.log(data)
                $('#edit-unit').find('.modal-body').html(data)
                $('#edit-unit').modal('show')
            },
            error: function (error) {
                console.log(error)
            }
        })
    })

    $('.btn-update').on('click', function () {
        // console.log($(this).data('id'))
        let id = $('#form-edit').find('#id_data').val()
        let formData = $('#form-edit').serialize()
        console.log(formData)
        // console.log(id)
        $.ajax({
            url: `/master/unit/update/${id}`,
            method: "POST",
            data: formData,
            success: function (data) {
                // console.log(data)
                // $('#edit-unit').find('.modal-body').html(data)
                $('#edit-unit').modal('hide')
                // window.location.assign('/unit')
                location.reload();
            },
            error: function (error) {
                console.log(error)
            }
        })
    })
    //EDIT & UPDATE SATUAN


    //DELETE SATUAN
    $(".swal-confirm").click(function (e) {
        id = e.target.dataset.id;
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $(`#delete${id}`).submit();
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });
    //DELETE SATUAN

    //MULTIPLE DELETE UNIT
    $("#chkCheckAll").click(function () {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
    });

    $("#deleteAllSelectedRecord").click(function (e) {
        id = e.target.dataset.id;
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $(`#delete${id}`).submit();

                    // e.preventDefault(e);
                    var allids = [];

                    $("input:checkbox[name=ids]:checked").each(function () {
                        allids.push($(this).val());
                    });

                    $.ajax({
                        url: `/unit/multipleDelete`,
                        type: "DELETE",
                        data: {
                            _token: $("input[name=_token]").val(),
                            ids: allids
                        },
                        success: function (response) {
                            $.each(allids, function (key, val) {
                                $("#sid" + val).remove();
                            })
                            location.reload();
                            // dataTable(true);
                        }
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }

            });
    });
    //MULTIPLE DELETE UNIT


    //EDIT & UPDATE KONVERSI
    $('.edit-konversi').on('click', function () {
        // console.log($(this).data('id'))

        let id = $(this).data('id')
        $.ajax({
            url: `/konversi/edit/${id}`,
            method: "GET",
            success: function (data) {
                // console.log(data)
                $('#edit-konversi').find('.modal-body').html(data)
                $('#edit-konversi').modal('show')
            },
            error: function (error) {
                console.log(error)
            }
        })
    })

    $('.update-konversi').on('click', function () {
        // console.log($(this).data('id'))
        let id = $('#form-edit-konversi').find('#id_data').val()
        let formData = $('#form-edit-konversi').serialize()
        console.log(formData)
        // console.log(id)
        $.ajax({
            url: `konversi/update/${id}`,
            method: "POST",
            data: formData,
            success: function (data) {
                // console.log(data)
                // $('#edit-unit').find('.modal-body').html(data)
                $('#edit-konversi').modal('hide')
                // window.location.assign('/unit')
                location.reload();
            },
            error: function (error) {
                console.log(error)
            }
        })
    })
    //EDIT & UPDATE KONVERSI

});

function dataTable(destroy = false) {
    if (destroy == true) {
        $('#my-table').DataTable().destroy();
    }
    $('#my-table').DataTable();

}