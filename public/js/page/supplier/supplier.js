const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "suppliertype.type_name", name: "suppliertype.type_name" },
    { data: "nama_supplier", name: "nama_supplier" },
    { data: "no_telepon", name: "no_telepon" },
    {
        data: "status",
        name: "status",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: 'created_at', name: 'created_at' },
    { data: 'updated_at', name: 'updated_at' },
    { data: "btn", name: "btn" },
];

$(function () {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/supplier/get" });

    $('table').DataTable();

    $("#delete").on("click", function () {
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: "/master/supplier/delete",
                        data: {
                            id: value_checkbox,
                            parm: 'withTrashed'
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });

    $("#table").on("change", ".status", function () {
        $.ajax({
            url: "/master/supplier/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('button[name="total"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/supplier/get" });
    });

    $('button[name="trashed"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/supplier/get", parm: { parm: 'trashed' } });
    });
    $('button[name="active"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/suppliertype/get", parm: { parm: 'status', value: 1 } });
    });
    $('button[name="inactive"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/suppliertype/get", parm: { parm: 'status', value: 0 } });
    });

    $("#insert-supplier").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/master/supplier/insert",
            data: new FormData(this),
            processData: false,
            contentType: false,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            beforeSend: function () {
                $('#insert-supplier').trigger("reset");
                $('#load-insert').html("Loading...");
                $('#load-insert').prop("disabled", true);
            },
            success: res => {
                $('#load-insert').prop("disabled", false);
                $('#load-insert').html("Save changes");
                RefreshTable("table");
                getAllData();
                $("#add-supplier").modal('hide');
                $('.modal-backdrop').remove();
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "2000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr.success('Data Berhasil Di Tambahkan !', 'Konversi Satuan')
            },
            error: err => console.log(err)
        });
    });

    $("#update-supplier").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/master/supplier/update",
            data: new FormData(this),
            processData: false,
            contentType: false,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            beforeSend: function () {
                $('#update-supplier').trigger("reset");
                $('#load-updated').html("Loading...");
                $('#load-updated').prop("disabled", true);
            },
            success: res => {
                $('#load-updated').prop("disabled", false);
                $('#load-updated').html("Save changes");
                RefreshTable("table");
                getAllData();
                $("#edit-supplier").modal("hide");
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "2000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr.success('Data Berhasil Di Edit !', 'Konversi Satuan');
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#edit", function (e) {
        e.preventDefault();
        let id = $(this).data('id')
        $.ajax({
            url: `/master/supplier/edit/${id}`,
            method: "GET",
            success: function (data) {
                // console.log(data)
                $('#edit-supplier').find('.modal-body').html(data)
                $('#edit-supplier').modal('show')
            },
            error: function (error) {
                console.log(error)
            }
        })
    });

    $("#table").on("click", "#delete", function (e) {
        e.preventDefault();
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: "/master/supplier/delete",
                        data: {
                            id: $(this).data("id")
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });

    $("#table").on("click", "#restore", function (e) {
        e.preventDefault();
        $.ajax({
            url: `/master/supplier/delete`,
            data: {
                id: $(this).data("id"),
                parm: 'restore'
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#deletePermanent", function (e) {
        e.preventDefault();
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: `/master/supplier/delete`,
                        data: {
                            id: $(this).data("id"),
                            parm: 'deletePermanent'
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });
})

function getAllData() {
    $.ajax({
        url: "/master/supplier/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}