const BaseURL = document.getElementsByName('baseUrl')[0].getAttribute('content');
const csrf_val = document.getElementsByName('csrf')[0].getAttribute('content');
$.noConflict();

var buttonSampah = $('#buttonSampah');
var buttonSoftSampah = $('#buttonSoftSampah');
// var tableMerk = $('#merk-table').DataTable().ajax();

$(function () {
    dataTables('merk-table', 'GET', '/tables/merk');

    // $('#merk-table_filter :input').on('keypress', function(){
    //     dataTables('merk-table','POST','/tables/merk/', true);
    // });

    // Select / Deselected Boxed
    $('#chkCheckAll').on('click', function (e) {
        // e.preventDefault();
        // e.stopPropagation();
        var ischeck = $(this).is(':checked');
        if (ischeck) {
            $('.case').each(function () {
                // $('.case').prop('checked', this.checked);
                $(this).prop('checked', true);
                buttonSampah.removeClass('disabled');
                buttonSoftSampah.removeClass('disabled');
            });
        } else {
            $('.case').each(function () {
                $(this).prop('checked', false);
                buttonSampah.addClass('disabled');
                buttonSoftSampah.addClass('disabled');
            });
        }
    });

    // Changing state of CheckAll checkbox 
    $('#merk-table > tbody').on('change', 'input[type="checkbox"]',function (e) {
        // e.preventDefault();
        // e.stopPropagation();
        if ($(this).length == $('.case:checked').length) {
            $('#chkCheckAll').attr('disabled',true);
            buttonSampah.removeClass('disabled');
            buttonSoftSampah.removeClass('disabled');
        } else {
            $('#chkCheckAll').attr('disabled',false);
            buttonSampah.addClass('disabled');
            buttonSoftSampah.addClass('disabled');
        }

    });

    // Form Simpan
    $('#simpanMerk').on('click',function(e){
        // alert('ok');
        e.preventDefault();
        var stats = '';

        stats = $('#statusMerek').is(':checked') ? '1' : '0';

        var formData = {
            merk_name : $('#namaMerek').val(),
            merk_code : $('#codeMerek').val(),
            created_by : $('#dibuatOleh').val(),
            status : stats,
        };
        $.ajax({
            type : 'POST',
            // url : BaseURL + '/master/merk/storeMerk',
            url : $('#formMerk').attr('action'),
            data : formData,
            dataType : 'json',
            encode:true,
            headers : {
                'X-CSRF-TOKEN' : csrf_val,
            },
            timeout: 20000,
            beforeSend: function(){
                $('#simpanMerk').prop('disabled',true);
            },
            success: function (response){
                console.log(response);
                // dataTables('merk-table', 'GET', '/tables/merk');
            },
            complete: function(result){
                // console.log(result);
                $('#merk-table').DataTable().ajax.reload(null, false);
                $('#simpanMerk').prop('disabled',false);
            }
        });

    });

    // Delete Button Request
    $('#merk-table > tbody').on('click', '.destroyMerk',function(e){
        // console.log($(this).data('id'));
        var id = $(this).data('id');
        $.ajax({
            type : 'DELETE', 
            url : BaseURL + '/master/merk/destroyMerk/' + id,
            // data : {
            //     id : id
            // },
            dataType : 'json',
            encode:true,
            headers : {
                'X-CSRF-TOKEN' : csrf_val,
            },
            timeout: 20000,
            beforeSend: function(){
                // $('#simpanMerk').prop('disabled',true);
            },
            success: function (response){
                console.log(response);
                // dataTables('merk-table', 'GET', '/tables/merk');
            },
            complete: function(result){
                // console.log(result);
                $('#merk-table').DataTable().ajax.reload(null, false);
                // $('#simpanMerk').prop('disabled',false);
            }
        });

    });

    $('#merk-table > tbody').on('click', '.statusMerk', function(e){
        var id = $(this).data('id');
        var stats = $(this).is(':checked') ? '1' : '0';
        // alert('id : ' + id + ' status : ' + stats);
        $.ajax({
            type : 'PATCH',
            // url : BaseURL + '/master/merk/storeMerk',
            url : BaseURL + '/master/merk/statusMerk/' + id,
            data : {
                status : stats
            },
            dataType : 'json',
            encode:true,
            headers : {
                'X-CSRF-TOKEN' : csrf_val,
            },
            timeout: 20000,
            // scrollY: '50v',
			// scrollX: '50h',
			// scrollCollapse: true,
			// scroller: {
			// 	loadingIndicator: true
			// },
            beforeSend: function(){
                $('#customSwitch'+id).prop('disabled',true);
            },
            success: function (response){
                console.log(response);
                // dataTables('merk-table', 'GET', '/tables/merk');
            },
            complete: function(result){
                // console.log(result);
                // $('#merk-table').DataTable().ajax.reload(null, false);
                $('#customSwitch'+id).prop('disabled',false);
            }
        });
    });

});

// async function changeState(id = null){
//     alert('ok');
// }

// Fungsi Datatable
async function dataTables(tableID = null, types = null, urls = null) {
    // console.log(tableID, types, urls);
    let tablesMerk = $(`#${tableID}`).DataTable({
        destroy : true,
        retrieve: true,
        processing: true,
        serverSide: true,
        searchDelay: 1000,
        stateSave: true,
        // scrollX:true,
        // scrollY:true,
        ajax: {
            headers: {
                'X-CSRF-TOKEN': csrf_val,
                // 'Content-Type': 'application/json'
            },
            crossDomain: true,
            type: types,
            url: BaseURL + urls,
            pages: 5
        },
        columns: [
            // { data: 'id', name: 'id', orderable: false, searchable:false},
            { data: 'boxed', name: 'boxed', orderable: false, searchable: false },
            { data: 'rownum', name: 'rownum', searchable: false },
            { data: 'merk_name', name: 'merk_name' },
            { data: 'merk_code', name: 'merk_code', orderable: false },
            { data: 'created_by', name: 'created_by', orderable: false },
            { data: 'created_at', name: 'created_at', searchable: false },
            { data: 'updated_at', name: 'updated_at', searchable: false },
            { data: 'checked', name: 'checked', orderable: false, searchable: false },
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ]
    });
    setInterval(function () {
        tablesMerk.ajax.reload(null, false); // user paging is not reset on reload
    }, 180000);

}

// $(function () {
//     console.log(BaseURL);
//     console.log(csrf_val);

//     $('#merk-table').DataTable({
//         processing: true,
//         serverSide: true,
//         order: false,
//         ajax: {
//             headers: {
//                 'X-CSRF-TOKEN': csrf_val,
//             },
//             crossDomain: true,
//             url: `${BaseURL}/api/merk`,
//             type: "GET",
//         },
//         columns: [
//             { "data": "id" },
//             { "data": "merk_name" },
//             { "data": "merk_code" },
//             { "data": "status" },
//             { "data": "created_by" },
//             { "data": "created_at" },
//             { "data": "id"}
//         ]
//     });

// });