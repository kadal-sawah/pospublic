const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "title", name: "title" },
    { data: "type.type_name", name: "type.type_name" },
    {
        data: "status",
        name: "status",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: 'created_at', name: 'created_at' },
    { data: 'updated_at', name: 'updated_at' },
    { data: "btn", name: "btn" }
];

$(function () {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/customer/get" });
    $("#delete").on("click", function () {
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: "/master/customer/delete",
                        data: {
                            id: value_checkbox,
                            parm: "withTrashed"
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });

    $("#table").on("change", ".status", function () {
        $.ajax({
            url: "/master/bincard/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('button[name="total"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/customer/get" });
    });

    $('button[name="trashed"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/customer/get", parm: { parm: 'trashed' } });
    });
    $('button[name="active"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/customer/get", parm: { parm: 'status', value: 1 } });
    });
    $('button[name="inactive"]').on("click", function () {
        Table({ table: "#table", data: data, url: "/master/customer/get", parm: { parm: 'status', value: 0 } });
    });

    $('#createNewCustomer').click(function () {
        $('#saveBtn').val("create-Customer");
        $('#Customer_id').val('');
        $('#CustomerForm').trigger("reset");
        $('#modelHeading').html("Create New Customer");
        $('#ajaxModel').modal('show');
    });
    $('body').on('click', '.editCustomer', function () {
        var Customer_id = $(this).data('id');
        $.get("" + '/master/customer/' + Customer_id + '/edit', function (data) {
            $('#modelHeading').html("Edit Customer");
            $('#saveBtn').val("edit-user");
            $('#ajaxModel').modal('show');
            $('#Customer_id').val(data.id);
            $('#title').val(data.title);
            $('#first_name').val(data.first_name);
            $('#last_name').val(data.last_name);
            $('#type_id').val(data.type_id);
            $('#address').val(data.address);
            $('#address_billing').val(data.address_billing);
            $('#address_delivery').val(data.address_delivery);
            $('#contact_name_1').val(data.contact_name_1);
            $('#contact_name_2').val(data.contact_name_2);
            $('#contact_name_3').val(data.contact_name_3);
            $('#contact_name_4').val(data.contact_name_4);
            $('#postal_code').val(data.postal_code);
            $('#telepon_1').val(data.telepon_2);
            $('#telepon_2').val(data.telepon_2);
            $('#facsimile').val(data.facsimile);
            $('#email').val(data.email);
            $('#bank_name').val(data.bank_name);
            $('#bank_rekening_number').val(data.bank_rekening_number);
            $('#bank_owner').val(data.bank_owner);
            $('#photo').val(data.photo);
        });
    });
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
            data: $('#CustomerForm').serialize(),
            url: `/master/customer/insert`,
            type: "POST",
            dataType: 'json',
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: function (data) {
                $('#CustomerForm').trigger("reset");
                $('#ajaxModel').modal('hide');
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
                $('#saveBtn').html('Save Changes');
            }
        });
    });

    // $("#insert-suppliertype").on("submit", function (e) {
    //     e.preventDefault();
    //     $.ajax({
    //         url: "/master/customer/insert",
    //         data: new FormData(this),
    //         processData: false,
    //         contentType: false,
    //         type: "POST",
    //         headers: {
    //             "X-CSRF-TOKEN": csrf_token
    //         },
    //         beforeSend: function () {
    //             $('#insert-suppliertype').trigger("reset");
    //             $('#load-insert').html("Loading...");
    //             $('#load-insert').prop("disabled", true);
    //         },
    //         success: res => {
    //             $('#load-insert').prop("disabled", false);
    //             $('#load-insert').html("Save changes");
    //             $('#insert-suppliertype').trigger("reset");
    //             RefreshTable("table");
    //             getAllData();
    //             $("#add-suppliertype").modal("hide");
    //             $('.modal-backdrop').remove();
    //             toastr.options = {
    //                 "closeButton": true,
    //                 "debug": false,
    //                 "newestOnTop": false,
    //                 "progressBar": true,
    //                 "positionClass": "toast-bottom-right",
    //                 "preventDuplicates": false,
    //                 "onclick": null,
    //                 "showDuration": "300",
    //                 "hideDuration": "1000",
    //                 "timeOut": "2000",
    //                 "extendedTimeOut": "1000",
    //                 "showEasing": "swing",
    //                 "hideEasing": "linear",
    //                 "showMethod": "fadeIn",
    //                 "hideMethod": "fadeOut"
    //             }
    //             toastr.success('Data Berhasil Di Tambahkan !', 'Konversi Satuan')
    //         },
    //         error: err => console.log(err)
    //     });
    // });

    // $("#update-suppliertype").on("submit", function (e) {
    //     e.preventDefault();
    //     $.ajax({
    //         url: "/master/customer/update",
    //         data: new FormData(this),
    //         processData: false,
    //         contentType: false,
    //         type: "POST",
    //         headers: {
    //             "X-CSRF-TOKEN": csrf_token
    //         },
    //         beforeSend: function () {
    //             $('#update-suppliertype').trigger("reset");
    //             $('#load-updated').html("Loading...");
    //             $('#load-updated').prop("disabled", true);
    //         },
    //         success: res => {
    //             $('#load-updated').prop("disabled", false);
    //             $('#load-updated').html("Save changes");
    //             RefreshTable("table");
    //             getAllData();
    //             $("#edit-suppliertype").modal("hide");
    //             toastr.options = {
    //                 "closeButton": true,
    //                 "debug": false,
    //                 "newestOnTop": false,
    //                 "progressBar": true,
    //                 "positionClass": "toast-bottom-right",
    //                 "preventDuplicates": false,
    //                 "onclick": null,
    //                 "showDuration": "300",
    //                 "hideDuration": "1000",
    //                 "timeOut": "2000",
    //                 "extendedTimeOut": "1000",
    //                 "showEasing": "swing",
    //                 "hideEasing": "linear",
    //                 "showMethod": "fadeIn",
    //                 "hideMethod": "fadeOut"
    //             }
    //             toastr.success('Data Berhasil Di Edit !', 'Konversi Satuan');
    //         },
    //         error: err => console.log(err)
    //     });
    // });

    // $("#table").on("click", "#edit", function (e) {
    //     e.preventDefault();
    //     $.ajax({
    //         url: "/master/customer/get/" + $(this).data("id"),
    //         type: "get",
    //         headers: {
    //             "X-CSRF-TOKEN": csrf_token
    //         },
    //         success: res => {
    //             $("#edit-suppliertype").modal("show");
    //             Object.keys(res.data).map(key => {
    //                 $(`#edit-suppliertype .form-control[name="${key}"]`).val(res.data[key]);
    //             });
    //         },
    //         error: err => console.log(err)
    //     });
    // });

    // $("#table").on("click", "#edit", function (e) {
    //     e.preventDefault();
    //     let id = $(this).data('id')
    //     $.ajax({
    //         url: `/master/customer/edit/${id}`,
    //         method: "GET",
    //         success: function (data) {
    //             // console.log(data)
    //             $('#edit-suppliertype').find('.modal-body').html(data)
    //             $('#edit-suppliertype').modal('show')
    //         },
    //         error: function (error) {
    //             console.log(error)
    //         }
    //     })
    // });

    $("#table").on("click", "#delete", function (e) {
        e.preventDefault();
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: "/master/customer/delete",
                        data: {
                            id: $(this).data("id")
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });

    $("#table").on("click", "#restore", function (e) {
        e.preventDefault();
        $.ajax({
            url: "/master/customer/delete",
            data: {
                id: $(this).data("id"),
                parm: 'restore'
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("click", "#deletePermanent", function (e) {
        e.preventDefault();
        swal({
            title: 'Yakin hapus data ?',
            text: 'Data yang udah di hapus gak bisa di balikin!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    // swal('Poof! Your imaginary file has been deleted!', {
                    //     icon: 'success',
                    // });
                    $.ajax({
                        url: `/master/customer/delete`,
                        data: {
                            id: $(this).data("id"),
                            parm: 'deletePermanent'
                        },
                        type: "DELETE",
                        headers: {
                            "X-CSRF-TOKEN": csrf_token
                        },
                        success: res => {
                            RefreshTable("table");
                            getAllData();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": true,
                                "positionClass": "toast-bottom-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Data Berhasil Di Hapus !', 'Konversi Satuan')
                        },
                        error: err => console.log(err)
                    });
                } else {
                    // swal('Your imaginary file is safe!');
                }
            });
    });
})

function getAllData() {
    $.ajax({
        url: "/master/customer/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}