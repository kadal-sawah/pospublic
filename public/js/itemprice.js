const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        ordering: false,
        sortable: false,
        searchable: false
    },
    { data: "item.item_name", name: "item.item_name" },
    { data: "branch.branch_name", name: "branch.branch_name" },
    { data: "current_price", name: "current_price" },
    { data: "discount", name: "discount" },
    { data: "discount_percent", name: "discount_percent" },
    { data: "user.name", name: "user.name" },
    {
        data: "status",
        name: "status",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "created_at", name: "created_at" },
    { data: "btn", name: "btn",
        orderable: false,
        ordering: false,
        sortable: false,
        searchable: false 
    }
];

$(function() {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/item/price/get" });

    $("#delete").on("click", function() {
        $.ajax({
            url: "/master/item/price/delete",
            data: {
                id: value_checkbox
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                Table({ table: "#table", data: data, url: "/master/item/price/get"});
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("change", ".status", function() {
        $.ajax({
            url: "/master/item/price/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('button[name="total"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/item/price/get"});
    });
    $('button[name="trashed"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/item/price/get", parm:{parm:'trashed'}});
    });
    $('button[name="active"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/item/price/get", parm:{parm:'status', value:1}});
    });
    $('button[name="inactive"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/item/price/get", parm:{parm:'status', value:0}});
    });
});

$("#insert-item-price").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/price/add",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        beforeSend: function(){
            $('#load').prop("disabled", true);
            $('#load').html("Loading...");
        },
        success: res => {
            location.reload(true);
        },
        error: err => console.log(err)
    });
});

$("#update-item-price").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/price/update",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            RefreshTable("table");
            getAllData();
            $('#edit-item-price').modal('hide');
            $('.modal-backdrop').remove();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#edit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/price/get/" + $(this).data("id"),
        type: "get",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            $("#edit-item-price").modal("show");
            Object.keys(res.data).map(key => {
                $(`#edit-item-price .form-control[name="${key}"]`).val(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#delete", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/price/delete",
        data: {
            id: $(this).data("id")
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/item/price/get"});
            //RefreshTable("item_price");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#restore", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/price/delete",
        data: {
            id: $(this).data("id"),
            parm : 'restore'
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/item/price/get", parm:{parm:'trashed'}});
            //RefreshTable("role");
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#deletePermanent", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/price/delete",
        data: {
            id: $(this).data("id"),
            parm : 'deletePermanent'
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/item/price/get", parm:{parm:'trashed'}});
            getAllData();
        },
        error: err => console.log(err)
    });
});

function getAllData() {
    $.ajax({
        url: "/master/item/price/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}

