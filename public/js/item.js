// $.noConflict();
const data = [
    {
        data: "check",
        name: "check",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "item_name", name: "item_name" },
    { data: "item_code", name: "item_code" },
    // { data: "item_group.group_name", name: "item_group.group_name" },
    { data: "photo", name: "photo" },
    { data: "merk.brand_name", name: "merk.brand_name" },
    { data: "consignment", name: "consignment" },
    { data: "user.name", name: "user.name" },
    {
        data: "status",
        name: "status",
        orderable: false,
        sortable: false,
        searchable: false
    },
    { data: "created_at", name: "created_at" },
    { data: "btn", name: "btn",
        orderable: false,
        ordering: false,
        sortable: false,
        searchable: false 
    }
];

$(function() {
    getAllData();
    Table({ table: "#table", data: data, url: "/master/item/get" });

    $("#delete").on("click", function() {
        $.ajax({
            url: "/master/item/delete",
            data: {
                id: value_checkbox
            },
            type: "DELETE",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                Table({ table: "#table", data: data, url: "/master/item/get"});
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $("#table").on("change", ".status", function() {
        $.ajax({
            url: "/master/item/change",
            data: {
                id: $(this).data("id"),
                status: $(this).val()
            },
            type: "PUT",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            success: res => {
                RefreshTable("table");
                getAllData();
            },
            error: err => console.log(err)
        });
    });

    $('button[name="total"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/item/get"});
    });
    $('button[name="trashed"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/item/get", parm:{parm:'trashed'}});
    });
    $('button[name="active"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/item/get", parm:{parm:'status', value:1}});
    });
    $('button[name="inactive"]').on("click", function() {
        Table({ table: "#table", data: data, url: "/master/item/get", parm:{parm:'status', value:0}});
    });
});

$("#insert-item").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/add",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        beforeSend: function(){
            $('#load').html("Loading...");
            $('#load').prop("disabled", true);
        },
        success: res => {
            // RefreshTable("table");
            // getAllData();
            // $('#add-item').modal('hide');
            // $('.modal-backdrop').remove();
            location.reload(true);
        },
        error: err => console.log(err)
    });
});

$("#update-item").on("submit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/update",
        data: new FormData(this),
        processData: false,
        contentType: false,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        beforeSend: function(){
            $('#load').html("Loading...");
            $('#load').prop("disabled", true);
        },
        success: res => {
            location.reload(true);
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#edit", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/get/" + $(this).data("id"),
        type: "get",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            $("#edit-item").modal("show");
            // $("#edit-item").html('<img src="/storage/'+res.data.photo+'" style="max-width: 350px; max-height: 250px;" class="img-fluid rounded-start" alt="..." />');
            $("#edit-item #pict").attr("src", "/storage/"+res.data.photo);
            Object.keys(res.data).map(key => {
                $(`#edit-item .form-control[name="${key}"]`).val(res.data[key]).attr("src", "/storage/"+res.data.photo);
            });
            // //Image preview
            // $('input[type="file"][name="product_image"]').on('change', function(){
            //     var img_path = $(this)[0].value;
            //     var img_holder = $('.img-holder');
            //     var extension = img_path.substring(img_path.lastIndexOf('.')+1).toLowerCase();
            //     if(extension == 'jpeg' || extension == 'jpg' || extension == 'png'){
            //          if(typeof(FileReader) != 'undefined'){
            //               img_holder.empty();
            //               var reader = new FileReader();
            //               reader.onload = function(e){
            //                   $('<img/>',{'src':e.target.result,'class':'img-fluid','style':'max-width:100px;margin-bottom:10px;'}).appendTo(img_holder);
            //               }
            //               img_holder.show();
            //               reader.readAsDataURL($(this)[0].files[0]);
            //          }else{
            //              $(img_holder).html('This browser does not support FileReader');
            //          }
            //     }else{
            //         $(img_holder).empty();
            //     }
            // });
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#delete", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/delete",
        data: {
            id: $(this).data("id")
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/item/get"});
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#restore", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/delete",
        data: {
            id: $(this).data("id"),
            parm : 'restore'
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/item/get", parm:{parm:'trashed'}});
            getAllData();
        },
        error: err => console.log(err)
    });
});

$("#table").on("click", "#deletePermanent", function(e) {
    e.preventDefault();
    $.ajax({
        url: "/master/item/delete",
        data: {
            id: $(this).data("id"),
            parm : 'deletePermanent'
        },
        type: "DELETE",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Table({ table: "#table", data: data, url: "/master/item/get", parm:{parm:'trashed'}});
            getAllData();
        },
        error: err => console.log(err)
    });
});

function getAllData() {
    $.ajax({
        url: "/master/item/all",
        headers: {
            "X-CSRF-TOKEN": csrf_token
        },
        success: res => {
            Object.keys(res.data).map(key => {
                $(`span[name="${key}"]`).text(res.data[key]);
            });
        },
        error: err => console.log(err)
    });
}
