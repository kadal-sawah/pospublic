let value_checkbox = [];
const  csrf_token = $('meta[name="csrf_token"]').data("token")

function Table(data) {
    $(data.table)
        .DataTable()
        .destroy();
    if (data.parm) {
        $(data.table).DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: data.url,
                data: data.parm
            },
            language: {
                search: "Search Data:",
                searchPlaceholder: "Search"
            },
            columns: data.data
        });
    } else {
        $(data.table).DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: data.url
            },
            language: {
                search: "Search Data:",
                searchPlaceholder: "Search"
            },
            columns: data.data
        });
    }

    $(`${data.table}_filter input`).removeClass("form-control-sm");
}


function checkbox_all(val) {
    var checkbox = $('#table input[name="checkbox-item"]');
    if (checkbox.length > 0) {
        for (i = 0; i < checkbox.length; i++) {
            if (val.checked) {
                checkbox[i].checked = true;
                value_checkbox.push(checkbox[i].value);
            } else {
                checkbox[i].checked = false;
                var index = value_checkbox.indexOf(checkbox[i].value);

                if (index > -1) {
                    value_checkbox.splice(index, 1);
                }
            }
        }
    } else {
        if (val.checked) {
            checkbox.checked = true;
        } else {
            checkbox.checked = false;
        }
    }
}

function checkbox_this(val) {
    if (val.checked) {
        $(this).checked = true;
        value_checkbox.push(val.value);
    } else {
        $(this).checked = false;
        var index = value_checkbox.indexOf(val.value);

        if (index > -1) {
            value_checkbox.splice(index, 1);
        }
    }
}


function RefreshTable(table) {
    table = $(`#${table}`).DataTable();
    table.ajax.reload();
}
