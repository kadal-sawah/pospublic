<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['branch_name', 'status', 'email', 'telepon', 'fax', 'address', 'created_by', 'deleted_at'];
    protected $dates = ['deleted_at'];
}
