<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Softdeletes;

class CustomerType extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['type_name', 'created_by', 'deleted_at'];
    protected $dates = ['deleted_at'];
}
