<?php

namespace App\Http\Controllers;

use App\Merk;
use Illuminate\Http\Request;

class MerkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'menuTitle' => 'Master Merk',
            'subMenuTitle' => 'Tabel Manajemen Merk',
            'breadcrumbs' => '<li class="breadcrumb-item active">Master Merk</li>',
            'collapseTitle' => 'Monitor Merk',
            'tableTitle' => 'Data Merk',
        ];

        return view('master.merk.merk',compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Merk::updateOrCreate(
            [
                'merk_name'     => $request->merk_name,
                'merk_code'     => $request->merk_code,
                'status'        => $request->status,
                'created_by'    => $request->created_by,
            ]
        );
        return response()->json(['success' => 'Merk data tersimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateState(Request $request, $id)
    {
        //
        // return $request;
        Merk::where('id',$id)->update(['status' => $request->status]);
        return response()->json(['success','Merk status diubah']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $merkId = Merk::findOrFail($id)->delete();
        return response()->json(['success','Merk data terhapus']);
    }
}