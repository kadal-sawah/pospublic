<?php

namespace App\Http\Controllers;

use App\Color;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ColorController extends Controller
{
    public function index(){
        return view('master.color.color');
    }
    
    public function colorGet()
    {
        if (empty($_GET['parm'])) {
            $data = Color::all();
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = Color::onlyTrashed();
            } else {
                $data = Color::where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group text-center">
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox text-center">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch text-center">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('Y/m/d | H:i:s');
            })

            ->addColumn('updated_at', function ($data) {
                return $data->updated_at->format('Y/m/d | H:i:s');
            })
            

            ->rawColumns(['btn', 'check', 'status','created_at', 'updated_at'])
            ->make(true);
    }

    public function colorInsert(Request $request)
    {
        Color::create($request->all());
    }

    public function getById($id)
    {
        $data = Color::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = Color::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = Color::where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Color::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = Color::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } else {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Color::withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = Color::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }
    }

    public function update(Request $request)
    {
        $data = Color::find($request->id);
        $data->update($request->all());
    }

    public function all()
    {
        $data['all']  = Color::all()->count();
        $data['active'] = Color::all()->where('status', 1)->count();
        $data['inactive'] = Color::all()->where('status', 0)->count();
        $data['trashed'] = Color::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = Color::find($request->id);
        $data->update(['status' => $status]);
    }
}
