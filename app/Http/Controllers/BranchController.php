<?php

namespace App\Http\Controllers;

use App\Branch;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BranchController extends Controller
{
    //
    public function index(){
        return view('master.branch');
    }


    public function get()
    {
        if (empty($_GET['parm'])) {
            $data = Branch::all();
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = Branch::onlyTrashed();
            } else {
                $data = Branch::where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group">
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            ->rawColumns(['btn', 'check', 'status'])
            ->make(true);
    }

    public function insert(Request $request)
    {
        Branch::create($request->all());
    }

    public function getById($id)
    {
        $data = Branch::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = Branch::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = Branch::where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Branch::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = Branch::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } else {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Branch::withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = Branch::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }
    }

    public function update(Request $request)
    {
        $data = Branch::find($request->id);
        $data->update($request->all());
    }

    public function all()
    {
        $data['all']  = Branch::all()->count();
        $data['active'] = Branch::all()->where('status', 1)->count();
        $data['inactive'] = Branch::all()->where('status', 0)->count();
        $data['trashed'] = Branch::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = Branch::find($request->id);
        $data->update(['status' => $status]);
    }
}
