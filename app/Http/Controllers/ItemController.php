<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Itemgroup;
use App\Merk;
use DB;
use File;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $merk =DB::table('merk')
                ->where('deleted_at', '=', null)
                ->where('status', '1')
                ->get();
        $item_group =DB::table('item_groups')
                    ->where('deleted_at', '=', null)
                    ->where('status', '1')
                    ->get();
        $item = DB::table('master_item')
            ->join('merk', 'merk.id', '=', 'master_item.brand_id')
            ->join('item_groups', 'item_groups.id', '=', 'master_item.item_group_id')
            ->select('master_item.*', 'merk.brand_name as nama_merk', 'item_groups.group_name as nama_item_group')
            ->get();

        return view('master.item', compact(['item', 'merk', 'item_group']));
        
    }

    public function get()
    {
        if (empty($_GET['parm'])) {
            $data = Item::with('user', 'item_group', 'merk');
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = Item::with('user', 'item_group', 'merk')->onlyTrashed();
            } else {
                $data = Item::with('user', 'item_group', 'merk')->where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group">
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            ->addColumn('photo', function ($data) { 
                $url=asset("storage/$data->photo"); 
                return '<img src='.$url.' style="max-width: 50px; max-height: 37px;" align="center" class="img-fluid rounded-start" alt="..." />'; 
            })
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('m/d/Y H:i:s') ;
            })
            ->rawColumns(['btn', 'check', 'status','photo'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getById($id)
    {
        $data = Item::with('item_group', 'merk')->find($id);
        // $url=asset("storage/$data->photo"); 
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function all()
    {
        $data['all']  = Item::all()->count();
        $data['active'] = Item::all()->where('status', 1)->count();
        $data['inactive'] = Item::all()->where('status', 0)->count();
        $data['trashed'] = Item::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $user_id = \Auth::user()->id;
            if($request->file('photo')){
            $file = $request->file('photo')->store('fotoItem','public');
            
            Item::create([
                'item_name' => $request->item_name,
                'item_code' => $request->item_code,
                'group_id' => '1',
                'item_group_id' => $request->item_group_id,
                'description' => $request->description,
                'brand_id' => $request->brand_id,
                'stock_min' => $request->stock_min,
                'stock_max' => $request->stock_max,
                'consignment' => $request->consignment,
                'status' => '1',
                'created_by' => $user_id,
                'photo'          => $file

            ]);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $data = Item::find($request->id);
        if( $request->file('ganti_photo')){
            Storage::delete('public/'.$data->photo);
            $file = $request->file('ganti_photo')->store('fotoItem','public');
            $data->photo = $file;
            $data->update([
                'photo'          => $file
            ]);
        }
        
        $data->update([
                'item_name' => $request->item_name,
                'item_code' => $request->item_code,
                'item_group_id' => $request->item_group_id,
                'description' => $request->description,
                'brand_id' => $request->brand_id,
                'stock_min' => $request->stock_min,
                'stock_max' => $request->stock_max,
                'consignment' => $request->consignment,
            ]);

        // $data->update($request->all());
        // $data->update($request->item_name);
        // $data->update([
        //     'item_name' => $request->item_name,
        //     'photo'     => $file
        // ]);

        // $data->item_name = $request->item_name;
        // $data->item_code = $request->item_code;
        // $data->group_id = $request->group_id;
        // $data->item_group_id = $request->item_group_id;
        // $data->description = $request->description;
        // $data->brand_id = $request->brand_id;
        // $data->stock_min = $request->stock_min;
        // $data->stock_max = $request->stock_max;
        // $data->consignment = $request->consignment;

        // $data->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = Item::find($request->id);
        $data->update(['status' => $status]);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = Item::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = Item::where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Item::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = Item::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } elseif ($request->parm == 'deletePermanent') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Item::withTrashed()->where('id', $value)->find($request->id);
                        Storage::delete('public/'.$data->photo);
                        $data->forceDelete();
                    }
                } else {
                    $data = Item::withTrashed()->where('id', $request->id)->find($request->id);
                    Storage::delete('public/'.$data->photo);
                    $data->forceDelete();
                }
            }

        }
    }

}
