<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerType;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function index(){

        // $province = DB::table('indonesia_provinces')
        //             ->get();

        // $city = DB::table('indonesia_cities')
        //             ->get();

        // $district = DB::table('indonesia_districts')
        //             ->get();
                    
        // $village = DB::table('indonesia_villages')
        //             ->get();

        // $data = array(
        //     // 'province' => $province,
        //     // // 'city' => $city,
        //     // 'district' => $district,
        //     // 'village' => $village,
        //     'customertype' => CustomerType::all(),
        // );

        $customertype = CustomerType::all();

        return view('master.customer.customer', compact('customertype'));
    }

    public function provinces()
    {
        return \Indonesia::allProvinces();
    }

    public function cities(Request $request)
    {
        return \Indonesia::findProvince($request->id, ['cities'])->cities->pluck('name', 'id');
    }

    public function districts(Request $request)
    {
        return \Indonesia::findCity($request->id, ['districts'])->districts->pluck('name', 'id');
    }

    public function villages(Request $request)
    {
        return \Indonesia::findDistrict($request->id, ['villages'])->villages->pluck('name', 'id');
    }
    
    public function customerGet()
    {
        if (empty($_GET['parm'])) {
            $data = Customer::with('type');
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = Customer::with('type')->onlyTrashed();
            } else {
                $data = Customer::with('type')->where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group text-center">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group text-center">
                        <a href="javascript:void(0)" type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning editCustomer">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="javascript:void(0)" type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </a>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox text-center">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            // ->addColumn('photo', function ($data) {
            //     $url=asset("storage/$data->photo");
            //     return '<img src='.$url.'style="max-width: 50px; max-height: 37px;" align="center"; class="img-fluid rounded-start" alt="..." />';
            // })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch text-center">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('Y/m/d | H:i:s');
            })

            ->addColumn('updated_at', function ($data) {
                return $data->updated_at->format('Y/m/d | H:i:s');
            })
            

            ->rawColumns(['btn', 'check', 'status','created_at', 'updated_at'])
            ->make(true);
    }

    public function customerInsert(Request $request)
    {
        // Customer::create($request->all());
        if($request->file('photo')) {
            $file = $request->file('photo')->store('fotoCustomer','public');
            
            Customer::updateOrCreate(['id' => $request->Customer_id],
                [
            	'title' => $request->title,
            	'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'type_id' => $request->type_id,
                'address' => $request->address,
                'address_billing' => $request->address_billing,
                'address_delivery' => $request->address_delivery,
                'contact_name_1' => $request->contact_name_1,
                'contact_name_2' => $request->contact_name_2,
                'contact_name_3' => $request->contact_name_3,
                'contact_name_4' => $request->contact_name_4,
                'postal_code' => $request->postal_code,
                'province_id' => $request->province_id,
                'city_id' => $request->city_id,
                'district_id' => $request->district_id,
                'village_id' => $request->village_id,
                'telepon_1' => $request->telepon_1,
                'telepon_2' => $request->telepon_2,
                'facsimile' => $request->facsimile,
                'email' => $request->email,
                'bank_name' => $request->bank_name,
                'bank_rekening_number' => $request->bank_rekening_number,
                'bank_owner' => $request->bank_owner,
                'status' => '1',
                'photo' => $file
            ]);
            
        }
    }

    public function getById($id)
    {
        $data = Customer::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function edit($id)
    {
        $Customer = Customer::findOrFail($id);
        return response()->json($Customer);
        // $data = array(
        //     'customer' => Customer::findOrFail($id),
        //     'customertype' => CustomerType::all(),
        // );

        return view ('master.customer.edit', $data);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = Customer::with('type')->where('id', $value);
                    $data->delete();
                }
            } else {
                $data = Customer::with('type')->where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Customer::with('type')->onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = Customer::with('type')->onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } else {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Customer::with('type')->withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = Customer::with('type')->withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }
    }

    public function update(Request $request)
    {
        $data = Customer::find($request->id);
        $data->update($request->all());
    }

    public function all()
    {
        $data['all']  = Customer::all()->count();
        $data['active'] = Customer::all()->where('status', 1)->count();
        $data['inactive'] = Customer::all()->where('status', 0)->count();
        $data['trashed'] = Customer::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = Customer::find($request->id);
        $data->update(['status' => $status]);
    }
}
