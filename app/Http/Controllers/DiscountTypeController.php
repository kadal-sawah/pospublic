<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DiscountType;
use App\User;
use DB;
use Yajra\DataTables\DataTables;

class DiscountTypeController extends Controller
{
    public function index()
    {
        return view('master.discountType');
    }

    public function get()
    {
        if (empty($_GET['parm'])) {
            $data = DiscountType::with('user');
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = DiscountType::with('user')->onlyTrashed();
            } else {
                $data = DiscountType::with('user')->where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group">
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('m/d/Y H:i:s') ;
            })
            ->rawColumns(['btn', 'check', 'status'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getById($id)
    {
        $data = DiscountType::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function all()
    {
        $data['all']  = DiscountType::all()->count();
        $data['trashed'] = DiscountType::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $user_id = \Auth::user()->id;
            DiscountType::create([
                'name' => $request->name,
                'percent' => $request->percent,
                'price' => $request->price,
                'value' => $request->value,
                'created_by' => $user_id,
            ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $data = DiscountType::find($request->id);
        // $data->update($request->all());

        $data->update([
                'name' => $request->name,
                'percent' => $request->percent,
                'price' => $request->price,
                'value' => $request->value,
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = DiscountType::find($request->id);
        $data->update(['status' => $status]);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = DiscountType::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = DiscountType::where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = DiscountType::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = DiscountType::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } elseif ($request->parm == 'deletePermanent') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = DiscountType::withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = DiscountType::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }
    }
}
