<?php

namespace App\Http\Controllers\Tables;

use App\Merk;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 
use Yajra\DataTables\DataTables;

class MerkDatatables extends Controller
{

    public function anyData(){

            DB::statement(DB::raw('set @rownum=0'));
            $merk = Merk::select([
                DB::raw('@rownum := @rownum + 1 AS rownum'),
                'id',
                'merk_name',
                'merk_code',
                'status',
                'created_by',
                'created_at',
                'updated_at']
                )->get();
            return Datatables::of($merk)
            ->editColumn('created_at',function($merk){
                return $merk->created_at->format('d-m-Y');
            })
            ->editColumn('updated_at',function($merk){
                return $merk->updated_at->format('H:i:s - d-m-Y');
            })
            ->addColumn('action',function($merk){
                $inner =  '<div class="btn-group btn-flat">';
                $inner .= '<a href="javascript: void(0)" class="btn btn-info btn-sm editMerk" data-id="'.$merk->id.'"><i class="far fa-edit"></i></a>';
                $inner .= '<a href="javascript: void(0)" class="btn btn-outline-info btn-sm ml-2" data-id="'.$merk->id.'"><i class="fas fa-eye"></i></a>';
                $inner .= '<a href="javascript: void(0)" class="btn btn-danger btn-sm ml-2 mr-2 destroyMerk" data-id="'.$merk->id.'"><i class="fas fa-trash"></i></a>';
                $inner .= '<a href="javascript: void(0)" class="btn btn-warning btn-sm softDeleteMerk" data-id="'.$merk->id.'"><i class="fas fa-trash-restore-alt"></i></a>';
                $inner .= '<div>';
                return $inner;
            })
            ->addColumn('boxed',function($merk){
                return '<input type="checkbox" class="case" data-id="'.$merk->id.'">';
            })
            ->addColumn('checked',function($merk){
                $checked = $merk->status == 1 ? 'checked' : '';
                $html = '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                            <input type="checkbox" class="custom-control-input statusMerk" ' . $checked . ' id="customSwitch' . $merk->id . '" value="' . $merk->status . '" data-id="' . $merk->id . '">
                            <label class="custom-control-label" for="customSwitch' . $merk->id . '"></label>
                        </div>';
                return $html;
            })
            ->rawColumns(['action','boxed','checked'])
            ->toJson();

    }

}
