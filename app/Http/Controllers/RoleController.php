<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    //
    public function index()
    {
        $data = DB::table('master_role')->get();
        return view('master.role', compact(['data']));
    }

    public function get()
    {
        if (empty($_GET['parm'])) {
            $data = Role::all();
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = Role::onlyTrashed();
            } else {
                $data = Role::where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group">
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            ->rawColumns(['btn', 'check', 'status'])
            ->make(true);
    }

    public function insert(Request $request)
    {
        Role::create($request->all());
    }

    public function getById($id)
    {
        $data = Role::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = Role::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = Role::where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Role::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = Role::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } else {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Role::withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = Role::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }
    }

    public function update(Request $request)
    {
        $data = Role::find($request->id);
        $data->update($request->all());
    }

    public function all()
    {
        $data['all']  = Role::all()->count();
        $data['active'] = Role::all()->where('status', 1)->count();
        $data['inactive'] = Role::all()->where('status', 0)->count();
        $data['trashed'] = Role::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = Role::find($request->id);
        $data->update(['status' => $status]);
    }
}
