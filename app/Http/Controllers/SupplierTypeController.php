<?php

namespace App\Http\Controllers;

use App\SupplierType;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SupplierTypeController extends Controller
{
    public function index(){
        return view('master.suppliertype.suppliertype');
    }
    
    public function suppliertypeGet()
    {
        if (empty($_GET['parm'])) {
            $data = SupplierType::all();
        } else {
            if (($_GET['parm']) == 'trashed') {
                $data = SupplierType::onlyTrashed();
            } else {
                $data = SupplierType::where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
            ->addColumn('btn', function ($data) {
                if (!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                    $btn = '<div class="btn-group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
                } else {
                    $btn = '<div class="btn-group text-center">
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
                }


                return $btn;
            })
            ->addColumn('check', function ($data) {
                return '<div class="custom-control custom-checkbox text-center">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
            })
            ->addColumn('status', function ($data) {
                $checked = $data->status == 1 ? 'checked' : '';
                return '<div class="custom-control custom-switch text-center">
                <input type="checkbox" class="custom-control-input status" ' . $checked . ' id="customSwitch' . $data->id . '" value="' . $data->status . '" data-id="' . $data->id . '">
                <label class="custom-control-label" for="customSwitch' . $data->id . '"></label>
              </div>';
            })
            
            ->editColumn('created_at', function ($data) {
                return $data->created_at->format('Y/m/d | H:i:s');
            })

            ->addColumn('updated_at', function ($data) {
                return $data->updated_at->format('Y/m/d | H:i:s');
            })
            

            ->rawColumns(['btn', 'check', 'status','created_at', 'updated_at'])
            ->make(true);
    }

    public function suppliertypeInsert(Request $request)
    {
        SupplierType::create($request->all());
    }

    public function getById($id)
    {
        $data = SupplierType::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function delete(Request $request)
    {
        if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = SupplierType::where('id', $value);
                    $data->delete();
                }
            } else {
                $data = SupplierType::where('id', $request->id);
                $data->delete();
            }
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = SupplierType::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = SupplierType::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } else {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = SupplierType::withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = SupplierType::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }
    }

    public function update(Request $request)
    {
        $data = SupplierType::find($request->id);
        $data->update($request->all());
    }

    public function all()
    {
        $data['all']  = SupplierType::all()->count();
        $data['active'] = SupplierType::all()->where('status', 1)->count();
        $data['inactive'] = SupplierType::all()->where('status', 0)->count();
        $data['trashed'] = SupplierType::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function change(Request $request)
    {
        $status = $request->status == 1 ? '0' : 1;
        $data = SupplierType::find($request->id);
        $data->update(['status' => $status]);
    }
}
