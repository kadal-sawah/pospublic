<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Clasification;
use Yajra\Datatables\Datatables;
use Session;

class ClasificationController extends Controller
{
    public function index()
    {
        $division = DB::table('clasifications')
        ->latest()
        ->get();
        
        $data = array (
            'division' => $division,
        );

        return view('master.clasification.clasification', $data);
    }

    public function clasificationGet()
    {
        if(empty($_GET['parm'])) {
            $data = Clasification::all();
        } else {
            if(($_GET['parm']) == 'trashed') {
                $data = Clasification::onlyTrashed();
            } else {
                $data = Clasification::where($_GET['parm'], $_GET['value'])->get();
            }
        }

        return DataTables::of($data)
        ->addColumn('btn', function ($data) {
            if(!empty($_GET['parm']) && $_GET['parm'] == 'trashed') {
                $btn = '<div class="btn-group" role="group">
                        <button type="button" id="restore" data-id="' . $data->id . '" class="btn btn-sm btn-info">
                            <i class="fas fa-recycle"></i>
                        </button>
                        <button type="button" id="deletePermanent" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </div>';
            } else {
                $btn = '<div class="btn-group" role="group" >
                        <button type="button" id="edit" data-id="' . $data->id . '" class="btn btn-sm btn-warning">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" id="delete" data-id="' . $data->id . '" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>';
            }

            return $btn;
        })
        ->addColumn('check', function ($data) {
            return '<div class="custom-control custom-checkbox text-center">
                <input class="custom-control-input" name="checkbox-item" value="' . $data->id . '" type="checkbox" id="customCheckbox' . $data->id . '" onchange="checkbox_this(this)">
                <label for="customCheckbox' . $data->id . '" class="custom-control-label"></label>
            </div>';
        })

        ->editColumn('created_at', function ($data) {
            return $data->created_at->format('Y/m/d | H:i:s');
        })

        ->addColumn('updated_at', function ($data) {
            return $data->updated_at->format('Y/m/d | H:i:s');
        })

        ->rawColumns(['btn', 'check', 'created_at', 'updated_at'])
        ->make(true);
    }

    public function clasificationInsert(Request $request)
    {
        Clasification::create($request->all());
    }

    public function getById($id)
    {
        $data = Clasification::find($id);
        return response()->json(['message' => 'query berhasil', 'status' => 'success', 'data' => $data], 200);
    }

    public function delete(Request $request)
    {
       if (empty($request->parm)) {
            if (is_array($request->id)) {
                foreach ($request->id as $value) {
                    $data = Clasification::where('id', $value);
                    $data->delete();
                }
            } else  {
                $data = Clasification::where('id', $request->id);
                $data->delete();
            } 
        } else {
            if ($request->parm == 'restore') {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Clasification::onlyTrashed()->where('id', $value);
                        $data->restore();
                    }
                } else {
                    $data = Clasification::onlyTrashed()->where('id', $request->id);
                    $data->restore();
                }
            } else {
                if (is_array($request->id)) {
                    foreach ($request->id as $value) {
                        $data = Clasification::withTrashed()->where('id', $value);
                        $data->forceDelete();
                    }
                } else {
                    $data = Clasification::withTrashed()->where('id', $request->id);
                    $data->forceDelete();
                }
            }

        }

    }

    public function update(Request $request) {
        $data = Clasification::find($request->id);
        $data->update($request->all());
    }

    public function all()
    {
        $data['all'] = Clasification::all()->count();
        $data['trashed'] = Clasification::withTrashed()->where('deleted_at', '!=', null)->count();

        return response()->json(['message' => 'query telah berhasil', 'status' => 'success', 'data' => $data], 200);
    }
}
