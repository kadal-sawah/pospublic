<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\SupplierType;

class Supplier extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['type_id', 'nama_supplier', 'no_telepon', 'alamat', 'status', 'created_by', 'deleted_at'];
    protected $dates = ['deleted_at'];

    public function suppliertype(){
        return $this->belongsTo(SupplierType::class,'type_id','id');
    }
}
