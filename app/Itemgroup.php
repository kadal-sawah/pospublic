<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Itemgroup extends Model
{
    protected $table = 'item_groups';
    protected $fillable = [
      'group_name', 'code_group', 'status', 'created_by', 'created_at', 'updated_at', 'deleted_at'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function user()
    {
      return $this->belongsTo(User::class, 'created_by', 'id');
    }
    
    public function item()
    {
      return $this->hasMany(Item::class);
    }
}
