<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shift extends Model
{
    //
    use softDeletes;
    protected $fillable = ['code_shift','name','deleted_at'];
    protected $dates = ['deleted_at'];
}
