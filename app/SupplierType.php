<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierType extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['type_name', 'status', 'created_by', 'deleted_at'];
    protected $dates = ['deleted_at'];

    public function Konversi(){
    	return $this->hasMany(Supplier::class);
    }
}
