<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Satuan;

class Konversi extends Model
{
    use SoftDeletes;
    protected $table = 'konversis';
    protected $fillable = ['unit_id','satuan_baru'];

    public function unit(){
        return $this->belongsTo(Satuan::class,'unit_id','id');
    }
}
