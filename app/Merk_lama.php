<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use App\Merk;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MerkDataTable extends DataTable
{
    protected $table = 'merks';
    // protected $fillable = [];
}