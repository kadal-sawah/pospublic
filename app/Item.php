<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    protected $table = 'master_item';
    protected $fillable = [
        'item_name', 'item_code', 'group_id', 'item_group_id', 'photo', 'description', 'brand_id', 'stock_min', 'stock_max', 'consignment', 'status', 'created_by'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function user()
    {
      return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function item_group()
    {
      return $this->belongsTo(Itemgroup::class, 'item_group_id', 'id');
    }

    public function merk()
    {
      return $this->belongsTo(Merk::class, 'brand_id', 'id');
    }
}
