<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
use App\Merk;
// use Yajra\DataTables\Html\Button;
// use Yajra\DataTables\Html\Column;
// use Yajra\DataTables\Html\Editor\Editor;
// use Yajra\DataTables\Html\Editor\Fields;
// use Yajra\DataTables\Services\DataTable;

class Merk extends Model
{
    protected $table = 'merks';
    protected $fillable = [
        'merk_name',
        'merk_code',
        'status',
        'created_by',
        'deleted_at',
        'created_at',
        'update_at',
    ];
}

// class Merk extends Model
// {
//     protected $table = 'merk';
//     protected $fillable = [
//       'brand_name', 'status', 'created_at', 'updated_at', 'deleted_at'
//     ];
//     use SoftDeletes;
//     protected $dates = ['deleted_at'];
    
//     public function item()
//     {
//       return $this->hasMany(Item::class);
//     }
// }
