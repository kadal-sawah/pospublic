<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model
{
    protected $table = 'discounts';
    protected $fillable = [
        'item_id', 'unit_id', 'discount_type', 'start_date', 'end_date', 'deleted_at', 'created_by', 'created_at', 'updated_at'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function user()
    {
      return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function item()
    {
      return $this->belongsTo(Item::class, 'item_id', 'id');
    }

    public function unit()
    {
      return $this->belongsTo(Satuan::class, 'unit_id', 'id');
    }

    public function discount_type()
    {
      return $this->belongsTo(DiscountType::class, 'discount_type', 'id');
    }

}
