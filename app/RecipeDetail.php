<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecipeDetail extends Model
{
    protected $table = 'recipe_details';
    protected $fillable = [
      'recipe_id', 'item_id', 'qty', 'uom_code', 'memo', 'created_by', 'created_at', 'updated_at', 'deleted_at'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function recipe()
    {
      return $this->belongsTo(Recipe::class, 'recipe_id', 'id');
    }

    public function user()
    {
      return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function item()
    {
      return $this->belongsTo(Item::class, 'item_id', 'id');
    }

    public function uom()
    {
      return $this->belongsTo(Satuan::class, 'uom_code', 'id');
    }
}
