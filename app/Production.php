<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Production extends Model
{
    protected $table = 'productions';
    protected $fillable = [
      'invoice', 'branch_id', 'description', 'amount', 'status', 'date', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function user()
    {
      return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function user_update()
    {
      return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function branch()
    {
      return $this->belongsTo(branch::class, 'branch_id', 'id');
    }
}
