<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Satuan extends Model
{
    use SoftDeletes;
    protected $table = 'unit';
    protected $fillable = ['name_unit','singkatan','status', 'deleted_at'];
    protected $dates = ['deleted_at'];

    public function Konversi(){
    	return $this->hasMany(Konversi::class);
    }
}
