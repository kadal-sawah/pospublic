<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Softdeletes;
use App\CustomerType;

class Customer extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['title', 'first_name', 'last_name', 'type_id', 'address',
                            'address_billing', 'address_delivery', 'contact_name_1', 'contact_name_2', 'contact_name_3',
                            'contact_name_4', 'postal_code', 'province_id', 'city_id', 'district_id', 'village_id',
                            'telepon_1', 'telepon_2', 'facsimile', 'email', 'bank_name', 'bank_rekening_number',
                            'bank_owner', 'status', 'photo','created_by', 'deleted_at'];
    protected $dates = ['deleted_at'];

    public function type(){
        return $this->belongsTo(CustomerType::class,'type_id','id');
    }

}
