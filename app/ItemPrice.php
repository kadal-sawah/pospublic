<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemPrice extends Model
{
    protected $table = 'item_prices';
    protected $fillable = [
      'item_id', 'branch_id', 'current_price', 'discount', 'discount_percent', 'discount_unit', 'discount_min','date_discount' , 'date_end_discount', 'status','created_by', 'created_at', 'updated_at', 'deleted_at'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function user()
    {
      return $this->belongsTo(User::class, 'created_by', 'id');
    }
    
    public function item()
    {
      return $this->belongsTo(Item::class, 'item_id', 'id');
    }

    public function branch()
    {
      return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }
}
